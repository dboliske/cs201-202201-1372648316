package Project1;

/* the class AgeRestrictionItem used to record the commodity in the store that has a restriction on purchasing based on age of the customer
 * the class AgeRestrictionItem inherit the class Commodity
 * the author: Min Wei, 16/04/2022
 */

public class AgeRestrictionItem extends Commodity {
	
	private int ageRestriction;  //the age restriction that the commodity has on purchasing
	
	public AgeRestrictionItem() { //default constructor
		super();
		ageRestriction = 21;
	}
	
	public AgeRestrictionItem(String name, String unit, double price, int ageRestriction) { //non-default constructor
		super(name, unit, price);
		this.ageRestriction = ageRestriction;
	}
	
	public AgeRestrictionItem(String name, String unit, double price, double quantities,int ageRestriction) { //non-default constructor
		super(name, unit, price,quantities);
		this.ageRestriction = ageRestriction;
	}

	public int getAgeRestriction() { //getter for ageRestriction
		return ageRestriction;
	}

	public void setAgeRestriction(int ageRestriction) { //setter for ageRestriction
		this.ageRestriction = ageRestriction;
	}

	public String toString() { //toString method
		return super.toString()+ ", age restriction: "+ageRestriction;
	}
	
	@Override
	public String toCSV() { //method for writing to the csv file
		return "agerestriction,"+super.toCSV()+ ","+ageRestriction;
	}
	
	public String type() { //method for getting the commodity's type
		return "agerestriction";
	}
	
	public boolean equals(Object obj) { //equals method
		if (!(super.equals(obj))) {
			return false;
		}
		
		AgeRestrictionItem a = (AgeRestrictionItem) obj;
		if(this.getAgeRestriction()!=a.getAgeRestriction()) {
			return false;
		}
		return true;
	}
}
