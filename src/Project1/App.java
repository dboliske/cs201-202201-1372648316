package Project1;

/*The programming is to model a general store. The customer can choose the commodities and add them to the cart and then check out through the programming
 * The customer can search the information of commodity by name too.
 * The clerk can create the items, sell the items, and modify the information of the items through the programming too.
 * The clerk can search the information of the items by different key words too.
 * The author: Min Wei, 22/4/2022
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class App {	
	
	static final String[] attributeName = {"Name","Measurement Unit","Unit Price","Quantities"};
	static String[] attributes = new String[attributeName.length];	
 
	public static ArrayList<Commodity> readFile(Scanner input,String filename){
	//method for loading the file if it exists
		ArrayList<Commodity> system = new ArrayList<Commodity>();
		try {
			File f = new File(filename);
			input = new Scanner(f);
			ProductItem p;
			ShelvedItem s;
			AgeRestrictionItem a;
			while(input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					String type = values[0].toLowerCase();
					switch(type) {
						case "product":
						case "productitem":
							p = new ProductItem(
									values[1],
									values[2],
									Double.parseDouble(values[3]),
									Double.parseDouble(values[4]),
									MyDate.parseDate(values[5])
									);
							system.add(p); 
							break;
						case "shelved":
						case "shelveditem":
							s = new ShelvedItem(
									values[1],
									values[2],
									Double.parseDouble(values[3]),
									Double.parseDouble(values[4])								
									);
							system.add(s); 
							break;
						case "agerestriction":
						case "agerestrictionitem":
							a = new AgeRestrictionItem(
									values[1],
									values[2],
									Double.parseDouble(values[3]),
									Double.parseDouble(values[4]),
									Integer.parseInt(values[5])
									);
							system.add(a); 
							break;	
					}		
				}catch(Exception e) {
				//when error occurred in reading a line, do nothing
				}
			}
		}catch (FileNotFoundException fnf) {
		//If there is not a file that exists, do nothing
		}catch (Exception e) {
		//Error occurred in reading in the file, print the error information
			System.out.println("Error occured in reading in file.");
		}
			    
		return system;
	}
	
	public static void printList(ArrayList<Commodity> list) {
	//method for printing the ArrayList of Commodity to the console		
		for(Commodity c: list) {
			System.out.println(c.toString());
		}
		System.out.println("");		
	}
	
		
	public static void demandCollection(Scanner input){
	//method for collecting the user's demand and then print to the console
		ArrayList<Commodity> demandList = new ArrayList<Commodity>();
		Commodity c = null;
		
		boolean complete;
		System.out.println("Please enter your demand information(enter exit to finish the input).");
		
		do {
			complete = false;	
			for(int i=0; i<attributeName.length; i++) {
				System.out.print("Commodity's "+attributeName[i]+": ");
				attributes[i] = input.nextLine();
				if(attributes[i].toLowerCase().equals("exit")) {
					complete = true;
					break;
				}								
			}
			System.out.println("");
			try {
				c = new Commodity(
						attributes[0],
						attributes[1],
						Double.parseDouble(attributes[2]),
						Double.parseDouble(attributes[3])				
						);	
				demandList.add(c);		
			} catch(Exception e) {
				System.out.println("Unavailable input, please enter again.");
			}
				
			
		} while(!complete);
		
		System.out.println("");		
		printList(demandList);
		System.out.println("");
	}
	
	public static void saveCart(String name,Cart cart) {
	//When users didn't check out and they want to keep the commodities in the cart, the method will save the cart as a file for them
		String filename = "src/Project1/"+name+".csv";
		try {
			FileWriter writer = new FileWriter(filename);
			writer.write(cart.toCSV()+"\n");
			writer.flush();
			writer.close();
			System.out.println("Your cart have been kept for you. Welcome back.");
		} catch(Exception e) {
			System.out.println("Error occurred in saving to the file.");
		}
		
	}
	
	public static ArrayList<Commodity> checkoutKeptCart(Scanner input,  ArrayList<Commodity> data){
	//method for the user to check out the cart that they have kept before
		System.out.print("Enter your name: ");
		String name = input.nextLine();
		String filename = "src/Project1/"+name+".csv";		
		Cart cart =readCart(input, filename);
		ArrayList<Commodity> cartList =	cart.getItems();
		ArrayList<Commodity> system = data;
		
		if(cartList.size()>0) {
			System.out.println("Please revise your cart before checking out now.");
			
			for(Commodity c: cartList) {
				System.out.println(c.toString());
				System.out.print("Do you want to revise it(Yes/No)? ");
				String yorn = input.nextLine().toLowerCase();
				if(yorn.equals("yes") || yorn.equals("y")){
					System.out.println("How many do you want to buy"+"("+c.getUnit()+") (quantities 0 represent giving up)?");
					System.out.print("Quantities: ");
					boolean completeEnter = false;
					do {
						String in = input.nextLine();
						System.out.println("");					
						try {
							double quantities = Double.parseDouble(in);	
							int index = system.indexOf(c);
							Commodity want = system.get(index);
							if (want.getQuantities()>=quantities) {
								c.setQuantities(quantities);
								completeEnter = true;
							} else {
								System.out.println("There are only "+c.getQuantities()+" "+c.getUnit()+" to sell now. Please enter again.");								
							}
							
						} catch(Exception e) {
							System.out.println("Unavailable input, Please enter again.");						
						}					
					} while(!completeEnter);
				}	
			}	
		}
		if (cartList.size()>0) {
			System.out.println("");
			system = checkout(cart, data);
		}		
		return system;
	}
	
	public static Cart readCart(Scanner input, String filename) {
	//method for the user to read in the cart that they have kept before	
		Commodity c;
		ArrayList<Commodity> list = new ArrayList<Commodity>();
		String name=null;
		try {
			File f = new File(filename);
			input = new Scanner (f);
			while(input.hasNextLine()) {
				String enter = input.nextLine();
				String[] sentence = enter.split(";");
				name = sentence[0];
				for(int i=1; i<sentence.length; i++) {
					String[] values = sentence[i].split(",");
					try {					
						c = new Commodity(
						values[0],
						values[1],
						Double.parseDouble(values[2]),
						Double.parseDouble(values[3])						
						);	
						list.add(c);		
							
					}catch(Exception e) {
					System.out.println("Error occured in reading in the kept data.");
					}
				}
				}
			} catch(Exception e) {
				System.out.println("Error occurred in reading in the file.");
			}				
				
			Cart cart = new Cart(name, list);		
			return cart;
	}
	
	
			
	public static ArrayList<Commodity> cart(Scanner input, ArrayList<Commodity> data){
	//method for the user to choose commodities and put them into the cart	
		
		System.out.print("Please enter your name:");
		String name = input.nextLine();	
		System.out.println("");
		Cart cart;	
		ArrayList<Commodity> system = data;
		
		ArrayList<Commodity> cartList = new ArrayList<Commodity>();
				
		Commodity item;
		for(Commodity c: system) {
			System.out.println(c.toString());
			System.out.print("Do you want it(Yes/No)? ");
			String yorn = input.nextLine().toLowerCase();			
			if(yorn.equals("yes") || yorn.equals("y")) {
				System.out.println("How many do you want to buy"+"("+c.getUnit()+")?");
				System.out.print("Quantities: ");
				boolean completeEnter = false;
				do {
					String in = input.nextLine();
					System.out.println("");					
					try {
						double quantities = Double.parseDouble(in);	
						if (c.getQuantities()>=quantities) {
							item = new Commodity(c.getName(), c.getUnit(), c.getPrice(), quantities);
							cartList.add(item);
							completeEnter = true;
						} else {
							System.out.println("There are only"+c.getQuantities()+c.getUnit()+" to sell now. Please enter again.");								
						}
						
					} catch(Exception e) {
						System.out.println("Unavailable input, Please enter again.");						
					}					
				} while(!completeEnter);
			}
		}
		
		cart = new Cart(name, cartList);
		System.out.print("Would you want to check out now? (Yes/No) ");			
		String choice = input.nextLine().toLowerCase();
		switch(choice) {
			case "yes":
			case "y":	
				system = checkout (cart, system);
				// check out and then ask whether the user want the home delivery service 								
				break;
			case "no":
			case "n":					
			// ask whether the user want the record in the cart is saved
				System.out.print("Do you want the record in the cart is kept for you? (Yes/No) ");
				String y = input.nextLine().toLowerCase();
				if(y.equals("yes") || y.equals("y")) {
					System.out.println(cart.toString());
					saveCart(name,cart);
				} else {}
				break;
		}		
		
		return system;
	}
			
		
	public static ArrayList<Commodity> checkout(Cart cart, ArrayList<Commodity> invotery){
	//method for the user to check out
		ArrayList<Commodity> system = invotery;
		ArrayList<Commodity> sell = cart.getItems();
		for (Commodity c: sell) {
			for(Commodity f: system) {
				if (c.equals(f)) {
					if (c.getQuantities()<=f.getQuantities()) {
						f.setQuantities(f.getQuantities()-c.getQuantities());
					} else {
						System.out.println("There are only"+f.getQuantities()+c.getUnit()+" to sell now.");
						f.setQuantities(0.0);
						c.setQuantities(f.getQuantities());
					}
					
				}
			}
			
		}
		System.out.println(cart.bill());
		System.out.println("");
		return system;
	}
	
	public static void searchCommodity(Scanner input, ArrayList<Commodity> data){
	//method for the user to search the commodity using the name
		ArrayList<Commodity> searchList = new ArrayList<Commodity>();
		
		System.out.println("What commodity do you want to search?");
		System.out.print("Commodity's Name: ");
		String name = input.nextLine();
		
		for(Commodity c: data) {
			if (c.getName().equals(name)) {
				searchList.add(c);				
			}
		}
		
		if(searchList.size()==0) {
			System.out.println("");
			System.out.println("Sorry, there is not the commodity "+name+" in our store.");
			System.out.println("");
		} else {
			System.out.println("");	
			printList(searchList);	
		}				
	}
	
	
	public static ArrayList<Error> errorInformation(Scanner input){
	/*method for collecting the wrong information that the user find
	 * When the user finish the enter, they will ask the clerk to decide whether to modify
	 */
		ArrayList<Error> errorList = new ArrayList<Error>();
		Error cow1= new Error();	
		
		String[] values = new String[4];
		System.out.println("Please enter the error information that you have found(enter exit to finish the input).");
		boolean complete = false;
		do {				
			System.out.print(cow1.getName()+": ");
			values[0]= input.nextLine();
			if(values[0].toLowerCase().equals("exit")) {
				complete = true;
				break;
			}	
			System.out.print(cow1.getIdentifier()+": ");
			values[1]= input.nextLine();
			if(values[1].toLowerCase().equals("exit")) {
				complete = true;
				break;
			}	
			System.out.print(cow1.getErrorInformation()+": ");
			values[2]= input.nextLine();
			if(values[2].toLowerCase().equals("exit")) {
				complete = true;
				break;
			}	
			System.out.print(cow1.getCorrectInformation()+": ");
			values[3]= input.nextLine();
			if(values[3].toLowerCase().equals("exit")) {
				complete = true;
				break;
			}	
			
			Error error = new Error(values[0],values[1],values[2],values[3]);
			errorList.add(error);
			System.out.println("");		
			
		} while(!complete);
		
		System.out.println("");
		for(Error s: errorList) {
			System.out.println(s.toString());			
		}
		System.out.println("");
		return errorList;		
		}
			
	
	
	public static ArrayList<Commodity> userMenu(Scanner input, ArrayList<Commodity> data){
	//method for the user's menu
		
		ArrayList<Commodity> system = data;
		
		boolean exit = false;
		do {
			System.out.println("Please choose:");
			String[] option = {"Collect demand","Choose commodities and add to the cart(including check out or keep the cart)",
					"Read in the kept cart and check out","Search commodity",
					"Provide error information(the clerk will modify the information according to them)","Exit"};
			for(int i=0; i<option.length; i++) {
				System.out.println((i+1)+"."+option[i]);
			}
			System.out.print("Enter your choice: ");
			String choice = input.nextLine();
			System.out.println("");
			System.out.println("");
			switch (choice){
				case "1":
					demandCollection(input);					
					break;
				case "2":
					system = cart(input, data);
					break;				
				case"3":
					system = checkoutKeptCart(input, data);
					break;
				case"4":
					searchCommodity(input, data);					
					break;
				case"5":
					ArrayList<Error> errorList = errorInformation(input);	
					system = modifyItems(errorList, input, data);
					break;
				case"6":
					System.out.println("Do you want to exit or switch the menu?");
					System.out.println("1.Switch to the clerk menu");
					System.out.println("2.Exit");
					System.out.print("Your choice: ");
					String ifExit = input.nextLine().toLowerCase();	
					System.out.println("");
					switch(ifExit) {
						case "1":							
							system = clerkMenu(input, system);
							exit = true;
							break;
						case "2":
							exit = true;
							break;
						default:
							System.out.println("Unavailbale choice. Accepted as exit.");
							exit = true;
							break;
					}
					break;
				default:
					System.out.println("unvailable choice, return the user menu.");					
			}
		}while(!exit);					
		return system;
	}
	
	
	public static ArrayList<Commodity> createItems(Scanner input, ArrayList<Commodity> data){
	//method for the clerk to create or add items
		ArrayList <Commodity> system = data;
		ProductItem p;
		ShelvedItem s;
		AgeRestrictionItem a;
		
		boolean complete = false;
		do {
			System.out.println("Please enter the item's information.");	
			for(int i=0; i<attributeName.length; i++) {
				System.out.print("Commodity's "+attributeName[i]+": ");
				attributes[i] = input.nextLine();			
			}
			System.out.print("The type of the Commodity: ");
			String type = input.nextLine().toLowerCase();
			switch(type) {
				case "productitem":
				case "product":
					//Enter expiration date  
					System.out.print("Expiration Date: ");
					String enter = input.nextLine();
					MyDate date = MyDate.parseDate(enter);
					System.out.println("");
					System.out.println("");                
					try {
						p = new ProductItem(
								attributes[0],
								attributes[1],
								Double.parseDouble(attributes[2]),
								Double.parseDouble(attributes[3]),				
								date);
						boolean have = false;
						for (Commodity c: system) {
							if(p.equals(c)) {
								have = true;
								c.setQuantities(Double.parseDouble(attributes[3])+c.getQuantities());
							}
						}
						if(!have) {
							system.add(p); 
						}
						complete = true;						
					}catch(Exception e) {
						System.out.println("");
						System.out.println("");  
						System.out.println("Unavailable input, please enter again.");
					}
					break;
				case "shelveditem":
				case "shelved":
					System.out.println("");
					System.out.println("");  
					try {
						System.out.println("");
						System.out.println("");        
						s= new ShelvedItem(
								attributes[0],
								attributes[1],
								Double.parseDouble(attributes[2]),
								Double.parseDouble(attributes[3])				
								);
						boolean have = false;
						for (Commodity c: system) {
							if(s.equals(c)) {
								have = true;
								c.setQuantities(Double.parseDouble(attributes[3])+c.getQuantities());
							}
						}
						if(!have) {
							system.add(s); 
						}
						complete = true;
					}catch(Exception e) {
						System.out.println("Unavailable input, please enter again.");
					}
					break;
				case "agerestriction":
				case "agerestrictionitem":
					try {
						System.out.print("Age Restriction: ");
						int age = Integer.parseInt(input.nextLine());
						System.out.println("");
						System.out.println("");        
						a = new AgeRestrictionItem(
								attributes[0],
								attributes[1],
								Double.parseDouble(attributes[2]),
								Double.parseDouble(attributes[3]),
								age
								);
						boolean have = false;
						for (Commodity c: system) {
							if(a.equals(c)) {
								have = true;
								c.setQuantities(Double.parseDouble(attributes[3])+c.getQuantities());
							}
						}
						if(!have) {
							system.add(a); 
						} 
						complete = true;
					}catch(Exception e) {
						System.out.println("Unavailable input, please enter again.");
					}
					break;
				default:
					System.out.println("Unavailable type, please enter again.");
			}
			
		} while(!complete);
		
		return system;
	}
	
	public static ArrayList<Commodity> sellItems(Scanner input, ArrayList<Commodity> data){
	//method for the clerk to sell items
		ArrayList <Commodity> system = data;
		System.out.println("What commodity do you want to sell?");
		System.out.print("Commodity's Name: ");
		String name = input.nextLine();
				
		boolean ifHave = false;
		for(Commodity c: data) {
			if (c.getName().equals(name)) {
				ifHave = true;
				System.out.println("How many do you want to sell"+"("+c.getUnit()+")?");
				System.out.print("Quantities: ");
				String in = input.nextLine();
				try {
					double quantities = Double.parseDouble(in);	
					if (c.getQuantities()>=quantities) {
						c.setQuantities(c.getQuantities()-quantities);
					} else {
						System.out.println("There are only "+c.getQuantities()+" "+c.getUnit()+" to sell now.");
						c.setQuantities(0.0);
					}
					
				} catch(Exception e) {
					System.out.println("Unavailable input, return to menu.");
					return system;
				}
			} 
		}
		if (!ifHave) {
			System.out.println("Sorry, We have not the Commodity.");
		}
		
		
		return system;
	}
	
	public static void searchItems(Scanner input, ArrayList<Commodity> data){
	//method for the clerk to search the information about items		
        ArrayList<Commodity> searchList = new ArrayList<Commodity>();
		
		System.out.println("What commodity do you want to search?");
		System.out.print("Commodity's Name: ");
		String name = input.nextLine();
		
		for(Commodity c: data) {
			if (c.getName().equals(name)) {
				searchList.add(c);				
			}
		}
		
		if(searchList.size()==0) {
			System.out.println("");
			System.out.println("Sorry, there is not the commodity "+name+" in our store.");
			System.out.println("");
		} else {
			System.out.println("");	
			printList(searchList);	
		}			
	}
	
	public static ArrayList<Commodity> modifyItems(ArrayList<Error> errors, Scanner input, ArrayList<Commodity> data){
	//method for the clerk to decide whether to modify the wrong information that the user have provided
		ArrayList<Commodity> system = data;
		ArrayList<Error> errorList = errors;
		
		for (Error e: errorList) {
			for(Commodity c: system) {
				if (e.getName().equals(c.getName())) {
					System.out.println(e);
					System.out.print("The information will be modified? (Yes/No) " );
					String choice = input.nextLine().toLowerCase();
					if (choice.equals("y")||choice.equals("yes")) {
						String identifier = e.getIdentifier().toLowerCase();
						switch(identifier) {
							case "name":
								c.setName(e.getCorrectInformation());
								break;
							case "unit":
								c.setUnit(e.getCorrectInformation());
								break;
							case "price":
								c.setPrice(Double.parseDouble(e.getCorrectInformation()));
								break;
							case "quantities":
								c.setQuantities(Double.parseDouble(e.getCorrectInformation()));
								break;
							case "expiration date":
								MyDate date = MyDate.parseDate(e.getCorrectInformation());
								c.setExpirationDate(date);
								break;
							case "age restriction":
								int age = Integer.parseInt(e.getCorrectInformation());
								c.setAgeRestriction(age);
								break;
							default:
								System.out.println("Error identifier. sorry, the information can't be modified.");
						}
					}
				}
			}
		}
			
		return system;		
	}
	
	public static ArrayList<Commodity> clerkMenu(Scanner input, ArrayList<Commodity> data){
	//method for the clerk's menu
		ArrayList<Commodity> system = data;
		boolean exit = false;
		do {
			System.out.println("Please choose:");
			String[] option = {"Create or Increase Items","Sell Items","Search Items","Exit"};
			for(int i=0; i<option.length; i++) {
				System.out.println((i+1)+"."+option[i]);
			}
			System.out.print("Your choice: ");
			String choice = input.nextLine();
			System.out.println("");
			System.out.println("");			
			switch (choice){
			case "1":
				system = createItems(input, data);
				break;
			case "2":
				system = sellItems(input, data);
				break;
			case"3":
				searchItems(input,data);				
				break;
			case"4":
				System.out.println("Do you want to exit or switch the menu?");
				System.out.println("1.Switch to the user menu");
				System.out.println("2.Exit");
				System.out.print("Your choice: ");
				String ifExit = input.nextLine().toLowerCase();	
				System.out.println("");
				switch(ifExit) {
					case "1":						
						system = userMenu(input, system);
						exit = true;
						break;
					case "2":
						exit = true;
						break;
					default:
						System.out.println("Unavailbale choice. Accepted as exit.");
						exit = true;
						break;
				}
				break;
			default:
				System.out.println("unvailable choice, return the menu.");				
		   }	
		}while(!exit);	
		return system;
	}
	
	public static ArrayList<Commodity> menu(Scanner input, ArrayList<Commodity> data){
	//method for the menu to choose to enter the user's menu or the clerk's menu
		System.out.println("Please choose:");
		System.out.println("1.User Menu");	
		System.out.println("2.Clerk Menu");
		System.out.println("3.Exit");
		System.out.print("Your choice: ");		
		String choice = input.nextLine();
		System.out.println("");
		System.out.println("");
		ArrayList<Commodity> system = new ArrayList<Commodity>();
		switch (choice){
			case "1":
			//load userMenu
				system = userMenu(input, data);
				break;
			case "2":
			//load clerkMenu
				system = clerkMenu(input, data);
				break;
			case "3":
			//exit
				break;
			default:
			//when choose unavailable choice, return the menu
				System.out.println("unvailable choice, return to the menu.");
				system = menu(input, data);
		}
		return system;		
	}
	
	public static void saveFile(Scanner input, ArrayList<Commodity> data) {
	//method for saving the file, when the programming finished.
		
		System.out.println("Please the file name for saving the data.");
		System.out.print("File Name: ");
		String filename = input.nextLine();
		try {
			FileWriter writer = new FileWriter(filename);
			for (Commodity c: data) {
				writer.write(c.toCSV()+"\n");
			}
			writer.flush();
			writer.close();
			System.out.println("Saving to file is finished.");
		} catch (Exception e) {
			System.out.println("Error occured in saving the file.");
		}		
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = input.nextLine();
		//Load file if it exists
		ArrayList<Commodity> system = readFile(input, filename);
		
		/*menu for choosing to enter the userMenu or clerkMenu
		 * You can choose to enter the user's menu or clerk's menu from here		* 
		 */
		system = menu(input, system);
		
		/*save file
		 * when finish the programming, the data generated from running the programming will be saved to a file
		 */
		saveFile(input, system);
				
		input.close();
		System.out.println("The programmming finish now.");
		                                                                                                                                                                                                                                                                                         

	}

}
