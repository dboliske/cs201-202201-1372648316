package Project1;

import java.util.ArrayList;

/* the class Cart used to record the commodities list that the customer add to the cart and the information of the customer
 *  the author: Min Wei,  17/04/2022
 */

public class Cart extends PurchasingList {
	
	
	private Customer customer; //the customer who purchase the commodity
	
	
	public Cart() { //default constructor		
		super();
		this.customer = new Customer();
	}
	
	
	public Cart(String name) { //default constructor		
		super();
		this.customer = new Customer(name);
	}
	
	
	public Cart(String name, int count) { //non-default constructor		
		super(count);	
		this.customer = new Customer(name);
	}	
	
	
	public Cart(String name, String telephone, int count) { //non-default constructor
		super(count);
		this.customer = new Customer(name,telephone);
			
	}
	
	
	public Cart(String name, String telephone, String address, int count) { //non-default constructor
		super(count);
		this.customer = new Customer(name,telephone,address);		
	}
	
	
	public Cart(Customer customer, int count) { //non-default constructor
		super(count);
		this.customer = customer;			
	}
	
	
	public Cart(String name, Commodity[] items) { //non-default constructor
		super(items);
		this.customer = new Customer(name);		
	}
	
	
	public Cart(String name,String telephone, Commodity[] items) { //non-default constructor
		super(items);
		this.customer = new Customer(name,telephone);		
	}
	
	
	public Cart(String name, String telephone, String address, Commodity[] items) { //non-default constructor
		super(items);
		this.customer = new Customer(name,telephone,address);		
	}	
	
		
	public Cart(Customer customer, Commodity[] items) { //non-default constructor
		super(items);
		this.customer = customer;			
	}
	
	
	public Cart(String name, ArrayList<Commodity> items) { //non-default constructor
		super(items);
		this.customer = new Customer(name);
	}
	
	
	public Cart(String name,String telephone, ArrayList<Commodity> items) { //non-default constructor
		super(items);
		this.customer = new Customer(name,telephone);
	}
	
	
	public Cart(String name,String telephone, String address, ArrayList<Commodity> items) { //non-default constructor
		super(items);
		this.customer = new Customer(name,telephone,address);
	}
	
	
	public Cart(Customer customer, ArrayList<Commodity> items) { //non-default constructor
		super(items);
		this.customer = customer;
	}
	
			
	public Customer getCustomer() {
		return customer;
	}
	
	public String getName() {
		return this.customer.getName();
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public ArrayList<Commodity> getItems() { //getter for items
		return super.getItems();
	}

	public void setItems(ArrayList<Commodity> items) { //setter for items
		super.setItems(items);
	}
	
	public double total() { //method for computing the total amount of the purchasing list
		return super.total();
    }	
		
	
	public String bill() { //method for providing the bill information
		String result = this.toString();
		result += "total amount: "+this.total();
		return result;
	}


	@Override
	public String toString() {
		String result = customer.getName()+"'s ";
		result += super.toString();		
		return result;
	}
	
	
	@Override
	public String toCSV() { //method for writing the customer's purchasing list to the csv file
		String result = customer.getName();
		result += ";"+super.toCSV();				
		return result;
	}
}
