package Project1;

/* the class Commodity used to record the commodity in the store
 * the author: Min Wei,   16/04/2022
 */

public class Commodity {
	
	private String name; //the commodity's name
	private String unit; //the commodity's measurement unit
	private double price; //the commodity's sell unit price
	private double quantities; //the commodity's quantities stored in the store
	
	public Commodity() { //default constructor
		name = "Commodity X";
		unit = "unit";
		price = 0.0;
		quantities = 0.0;
	}

	public Commodity(String name,String unit) { //non-default constructor
		this.name = name;
		this.unit = unit;
		price = 0.0;
		quantities = 0.0;
	}
	
	public Commodity(String name,String unit, double price) { //non-default constructor
		this.name = name;
		this.unit = unit;
		this.price = 0.0;
		setPrice(price);		
		quantities = 0.0;
	}
	
	public Commodity(String name,String unit, double price, double quantities) { //non-default constructor
		this.name = name;
		this.unit = unit;
		this.price = 0.0;
		setPrice(price);
		this.quantities = 0.0;
		setQuantities(quantities);
	}

	public String getName() { //getter for name
		return name;
	}

	public void setName(String name) { //setter for name
		this.name = name;
	}

	public String getUnit() {  //getter for unit
		return unit;
	}

	public void setUnit(String unit) { //setter for unit
		this.unit = unit;
	}

	public double getPrice() { //getter for price
		return price;
	}

	public void setPrice(double price) {  //setter for price
		if (price>0) {
			this.price = price;
		}
	}

	public double getQuantities() { //getter for quantities
		return quantities;
	}

	public void setQuantities(double quantities) {  //setter for quantities
		if(quantities>=0) {
			this.quantities = quantities;
		}
	}
	
	public void setAgeRestriction(int ageRestriction) {}
	
	public void setExpirationDate(MyDate expirationDate) {}
	
	public String toString() {  //toString method
		String result = "name: "+name+", unit: "+unit+", price: "+price+", quantities: "+quantities;
		return result;
	}
	
	public boolean equals(Object obj) { //equals method
		if(!(obj instanceof Commodity)) {
			return false;
		}
		
		Commodity c = (Commodity) obj;
		if(!(c.getName().equals(name))) {
			return false;
		} else if (!(c.getUnit().equals(unit))) {
			return false;
		} else if(!(c.getPrice()==price)) {
			return false;
		}
		return true;
	}
	
	public String toCSV() { //method for writing to the csv file
		String result = name+","+unit+","+price+","+quantities;
		return result;
	}
}
