package Project1;

/* the class Customer used to record the customer information of the store 
 * the author: Min Wei, 16/04/2022
 */

import java.util.regex.Pattern;

public class Customer {

	private String name; //the customer's name
	private String telephoneNumber; //the customer's telephone number
	private String address; //the customer's address for delivery
	private int age; //the customer's age
	
	public Customer() { //default constructor
		name = "Customer X";
		telephoneNumber = "11111111111";
		address = "";
		age = 0;
	}
	
	public Customer(String name) {  //non-default constructor
		this.name = name;
		telephoneNumber = "11111111111";
		address = "";
		age = 0;
	}
	
	public Customer(String name, String telephone) { //non-default constructor
		this.name = name;
		telephoneNumber = "11111111111";
		setTelephoneNumber(telephone);
		address = "";
		age = 0;
	}
	
	public Customer(String name, String telephone, String address) { //non-default constructor
		this.name = name;
		telephoneNumber = "11111111111";
		setTelephoneNumber(telephoneNumber);
		this.address = address;
		age = 0;
	}
	
	public Customer(String name, int age) { //non-default constructor
		this.name = name;
		telephoneNumber = "11111111111";
		address = "";
		this.age = age;
	}

	public String getName() { //getter for name
		return name;
	}

	public void setName(String name) { //setter for name
		this.name = name;
	}

	public String getTelephoneNumber() { //getter for telephoneNumber
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) { //setter for telephoneNumber
		if(Pattern.matches("\\d{11}", telephoneNumber)) {
			this.telephoneNumber = telephoneNumber;
		}
		this.telephoneNumber = telephoneNumber;
	}

	public String getAddress() {  //getter for address
		return address;
	}

	public void setAddress(String address) {  //setter for address
		this.address = address;
	}

	public int getAge() { //getter for age
		return age;
	}

	public void setAge(int age) { //setter for age
		this.age = age;
	}
	
	public String toString() { //toString method
		String result = "Name: "+name+"  Age:"+ (age>0?age:"unavailable")+"\\n";
		result += "Telephone Number: "+telephoneNumber +"\\n"+"Address: "+address;
		return result;				
	}
	
	public String toCSV() { //method for writing to the csv file
		return name + ","+ age + ","+ telephoneNumber +"," + address;
	}
	
	public boolean equals(Object obj) { //equals method
		if (!(obj instanceof Customer)) {
			return false;
		}
		
		Customer c = (Customer) obj;
		if (!(c.getName().equals(this.name))) {
			return false;
		} else if (!(c.getTelephoneNumber().equals(this.telephoneNumber))) {
			return false;
		} else if(!(c.getAddress().equals(this.address))) {
			return false;
		} else if (c.getAge()!=this.age) {
			return false;
		}
		
		return true;
	}
	
	public boolean underAge(int age) { //method for finding whether the customer's age is under some age 
		if(this.age<=age) {
			return false;
		}
		return true;
	}
	
}
