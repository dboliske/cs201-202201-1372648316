package Project1;

public class Error {
/* the class Error used to record the error information that have  been found 
 * the author: Min Wei, 24/04/2022
 */	
	
	protected String name;  //item name
	protected String identifier;  //error attribute's name
	protected String errorInformation; //error information
	protected String correctInformation; //correct information
	
	public Error() { //default constructor
		name = "Commodity's Name";
		identifier = "Identifier";
		errorInformation ="Error Information";
		correctInformation = "Correct Ihformation";		
	}
	
	public Error(String name, String identifier, String error, String correct) { //non-default constructor
		this.name = name;
		this.identifier = identifier;
		this.errorInformation = error;
		this.correctInformation = correct;
	}

	
	public String getName() { //getter for name
		return name;
	}

	public void setName(String name) { //setter for name
		this.name = name;
	}

	public String getIdentifier() { //getter for identifier
		return identifier;
	}

	public void setIdentifier(String identifier) { //setter for identifier
		this.identifier = identifier;
	}

	public String getErrorInformation() {  //getter for errorInformation
		return errorInformation;
	}

	public void setErrorInformation(String errorInformation) { //setter for errorInformation
		this.errorInformation = errorInformation;
	}

	public String getCorrectInformation() { //getter for correctInformation
		return correctInformation;
	}

	public void setCorrectInformation(String correctInformation) { //setter for correctInformation
		this.correctInformation = correctInformation;
	}
	
	public String toString() { //toString method
		return "Commodity's Name: "+ this.name +"," +"Identifier: "+this.identifier +","+"Error Information: "+ this.errorInformation + "," +"Correct Ihformation: "+this.correctInformation;
	}
	
}
