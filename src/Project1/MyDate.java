package Project1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class MyDate {
	/* the Class MyDate used to record the date
	 * the author: Min Wei, 24/04/2022
	 */	
	
	private int year; //the year in the date
	private Month month;  //the month in the date	
	private int day; //the day in the date
	
	//the Regex for the date
	private static String datePattern = 
			"([0-9]|[0][0-9]|[12][0-9]|[3][01])(-|/)"
			+"([1-9]|[0][1-9]|[1][0-2])(-|/)"
			+"[0-9]{4}";
	

	public MyDate() { //default constructor
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String today = simpleDateFormat.format(date);
		String[] values = today.split("/");
		this.day = Integer.parseInt(values[0]);
		int m = Integer.parseInt(values[1]);
		this.month = Month.getMonth(m);
		this.year = Integer.parseInt(values[2]);		
	}
	
	public MyDate(int year, int month, int day) { //non-default constructor
		this();
		setYear(year);
		setMonth(month);
		setDay(day);		
	}
	
	public MyDate(int year, Month month, int day) { //non-default constructor
		this();
		setYear(year);
		this.month = month;
		setDay(day);
	}
	
	public MyDate(String d) { //non-default constructor
		this();
		if(Pattern.matches(datePattern, d)) {
			String[] values = d.split("-|/");
			setYear(Integer.parseInt(values[2]));
			setMonth(Integer.parseInt(values[1]));
			setDay(Integer.parseInt(values[2]));
		}
	}

	public int getYear() { //getter for year
		return year;
	}

	public void setYear(int year) { //setter for year
		this.year = year;
	}

	public Month getMonth() { //getter for month
		return month;
	}
	
	public void setMonth(int month) { //setter for month
		Month m = Month.getMonth(month);
		if(m!=null) {
			this.month = m;
		}
	}

	public void setMonth(Month month) { //another setter for month
		this.month = month;
	}

	public int getDay() { //getter for day
		return day;
	}

	public void setDay(int day) { //setter for day
		if(day>=1 && day<=month.numberOfDays()) {
			this.day = day;
		}					
	}
	
	public String toString() { //toString method
		return day+"/"+month.number()+"/"+ year;		
	}
	
	public boolean equals(Object obj) { //equals method
		if (this == obj) {
			return true;			
		} else if (obj == null) {
			return false;					
		} else if(! (obj instanceof MyDate)) {
			return false;
		}
		
		MyDate d = (MyDate) obj;
		if(year!= d.getYear()) {
			return false;
		} else if (month != d.getMonth()) {
			return false;
		} else if (day!=d.getDay()) {
			return false;			
		}
		
		return true;
	}
	
	
	public static MyDate parseDate(String d) { //method for type cast the String to MyDate
		if(Pattern.matches(datePattern, d)) {
			return new MyDate(d);
		}
		return null;
	}
	
	
}


