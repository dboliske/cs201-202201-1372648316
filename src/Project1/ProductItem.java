package Project1;

/* the class ProductItem used to record the commodity in the store that are fresh and will expire if not sold
 * the class ProductionItem inherit the class Commodity
 * the author: Min Wei,  16/04/2022
 */

public class ProductItem extends Commodity {

	private MyDate expirationDate;  //the commodity's expiration date
	
	public ProductItem() {  //default constructor
		super();
		expirationDate = new MyDate();
	}
	
	public ProductItem(String name, String unit, double price) { //non-default constructor
		super(name,unit,price);
		expirationDate = new MyDate();
	}
	
	public ProductItem(String name, String unit, double price, MyDate expirationDate) { //non-default constructor
		super(name,unit,price);
		this.expirationDate = expirationDate;
	}
	
	public ProductItem(String name, String unit, double price, double quantities) { //non-default constructor
		super(name,unit,price,quantities);
		expirationDate = new MyDate();
	}
	
	public ProductItem(String name, String unit, double price, double quantities, MyDate expirationDate) { //non-default constructor
		super(name,unit,price,quantities);
		this.expirationDate = expirationDate;
	}
	
	public MyDate getExpirationDate() { //getter for expirationDate
		return expirationDate;
	}

	public void setExpirationDate(MyDate expirationDate) { //setter for expirationDate
		this.expirationDate = expirationDate;
	}
	
	public String toString() {  //toString method
		return super.toString()+", expiration date: "+expirationDate;
	}
	
	@Override
	public String toCSV() { //method for writing to the csv file
		return "product,"+super.toCSV()+","+ expirationDate;
	}
	
	public String type() { //method for getting the commodity's type
		return "product";
	}
	
	public boolean equals(Object obj) { //equals method
		if (!(super.equals(obj))){
			return false;
		}
		
		ProductItem p = (ProductItem)obj;
		if (!this.getExpirationDate().equals(p.getExpirationDate())) {
			return false;
		}
		return true;
	}
		
}
