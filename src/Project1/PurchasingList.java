package Project1;

/* the class PurchasingList used to record the customer's purchasing list 
 * the author: Min Wei, 17/04/2022
 */

import java.util.ArrayList;

public class PurchasingList {
	
	private ArrayList<Commodity> items;  //the commodities that are purchased	
	
	
	public PurchasingList() { //default constructor		
		items = new ArrayList<Commodity>();		
	}
	
	
	public PurchasingList(int count) { //non-default constructor		
		items = new ArrayList<Commodity>(count);		
	}	
	
	
	public PurchasingList(Commodity[] items) { //non-default constructor		
		this.items= new ArrayList<Commodity>(items.length);
		for(int i=0; i<items.length; i++) {
			this.items.add(items[i]);
		}		
	}	
		
	
	public PurchasingList(ArrayList<Commodity> items) { //non-default constructor		
		this.items= new ArrayList<Commodity>(items.size());
		for(Commodity c : items) {
			this.items.add(c);
		}		
	}
	
		
	public ArrayList<Commodity> getItems() { //getter for items
		return items;
	}

	public void setItems(ArrayList<Commodity> items) { //setter for items
		this.items = items;
	}

		
	public String toString() { //toString method		
		String result="Purchasing List:\n";
		for(Commodity c:items) {
			result += c.toString()+"\n";
		}
		return result;
	}
	
	public String toCSV() { //method for writing the customer's purchasing list to the csv file
		String result = items.get(0).toCSV();		
		for(int i=1;i<items.size();i++) {
			result += ";"+items.get(i).toCSV();
		}
		return result;
	}
	
	public double total() { //method for computing the total amount of the purchasing list
		double total = 0.0;
		for (Commodity c : items) {
			total= total + (c.getPrice()*c.getQuantities());
		}
		return total;
	}
	
	public String bill() { //method for providing the bill information
		String result = this.toString();
		result += "total amount: "+this.total();
		return result;
	}
	
	
}
