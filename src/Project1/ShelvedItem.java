package Project1;

public class ShelvedItem extends Commodity {
	
	public ShelvedItem() { //default constructor
		super();
	}

	public ShelvedItem(String name,String unit) { //non-default constructor
		super(name, unit);
	}
	
	public ShelvedItem(String name,String unit, double price) { //non-default constructor
		super(name, unit, price);
	}
	
	public ShelvedItem(String name,String unit, double price, double quantities) { //non-default constructor
		super(name, unit, price, quantities);
	}
	
	@Override
	public String toCSV() {
		return "shelved,"+super.toCSV();
	}
	
	public String type() { //method for getting the commodity's type
		return "shelved";
	}

	public boolean equals(Object obj) { //equals method
		if (!(super.equals(obj))){
			return false;
		}
		
		return true;
	}
}
