package Project1;

import java.util.ArrayList;
import java.util.Date;

/* the class SoldList used to record the purchasing list that have completed the purchasing and paid the bill
 * the author: Min Wei,  17/04/2022
 */

public class SoldList extends Cart {

	private Date purchaseDate; //the date when the purchasing occurs
	
	public SoldList() { //default constructor
		super();
		purchaseDate = new Date();
	}
	
	public SoldList(String name, int count) { //non-default constructor
		super(name, count);
		purchaseDate = new Date();
	}	
	
	public SoldList(String name, String telephone, int count) { //non-default constructor
		super(name, telephone, count);
		purchaseDate = new Date();
	}	
	
	public SoldList(String name, String telephone, String address, int count) { //non-default constructor
		super(name, telephone, address, count);
		purchaseDate = new Date();
	}	
	
	public SoldList(Customer customer, int count) { //non-default constructor
		super(customer, count);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, Commodity[] items) { //non-default constructor
		super(name, items);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, String telephone, Commodity[] items) { //non-default constructor
		super(name, telephone, items);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, String telephone, String address, Commodity[] items) { //non-default constructor
		super(name, telephone, address, items);
		purchaseDate = new Date();
	}
	
	public SoldList(Customer customer, Commodity[] items) { //non-default constructor
		super(customer, items);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, ArrayList<Commodity> items) { //non-default constructor
		super(name, items);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, String telephone, ArrayList<Commodity> items) { //non-default constructor
		super(name, telephone, items);
		purchaseDate = new Date();
	}
	
	public SoldList(String name, String telephone, String address, ArrayList<Commodity> items) { //non-default constructor
		super(name, telephone, address, items);
		purchaseDate = new Date();
	}
	
	public SoldList(Customer customer, ArrayList<Commodity> items) { //non-default constructor
		super(customer, items);
		purchaseDate = new Date();
	}
	
	public SoldList(Cart c) { //non-default constructor
		super(c.getCustomer(), c.getItems());
		purchaseDate = new Date();
	}
	
	public String deliveryAddress() { //method for providing the delivery address 
		String result = "Name: " + this.getCustomer().getName() + "\n";
		result += "The Delivery Address:\n";
		result += this.getCustomer().getAddress();
		return result;
	}

	public Date getPurchaseDate() { //getter for purchaseDate
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) { //setter for purchaseDate
		this.purchaseDate = purchaseDate;
	}	
	
	@Override
	public String toString() { //tpString method
		String result = super.toString()+"\n";
		result += "Total Amount: "+this.total();
		result += "\nsell date: "+ this.purchaseDate;
		return result;
	}
	
	public String toReport() { // method for writing the sell information to the csv file
		return  super.toCSV()+";"+this.purchaseDate ;
	}
	
	public boolean sellbefore (Date date) { //method for deciding whether the sell occurs before some a date
		if (this.purchaseDate.compareTo(date)>=0) {
			return false;
		}
		return true;
	}
	
	public boolean sellAfter (Date date) { //method for deciding whether the sell occurs after some a date
		if (this.purchaseDate.compareTo(date)<=0) {
			return false;
		}
		return true;
	}
		
}
