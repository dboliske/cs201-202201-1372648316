package exams.first;

import java.util.Scanner;

public class ArrayImprove {
	
	//method for prompting the user for words and store them in an array
	public static String[] acquireInput(Scanner input, int size) {
		String[] enters = new String[size];
		System.out.println("Please enter "+size+" words in the following.");
		for(int i=0; i<size; i++) {
			System.out.print("Enter word "+(i+1)+ " : ");
            enters[i]=input.nextLine();
		}
		return enters;
	}
	
	public static String[] resize(String[] array, int size) {
		String[] temp = new String[size];
		int limit = (array.length<size) ? array.length : size;
		for (int i=0; i<limit; i++) {
			temp[i]=array[i];
		}
		return temp;
	}
	
   
	public static String[] removeRepeat(String[] data) {
		String[] temp = new String[data.length-1];
		int count = 0;
		String firstWord = data[0];
		boolean hasRepeat = false;
		for(int i=1; i<data.length; i++) {
			if (!data[i].equals(firstWord)){
				temp[count] = data[i];
				count++;
			}else if(data[i].equals(firstWord)) {
				hasRepeat = true;
			}
		}
		if (hasRepeat) {
			System.out.println(firstWord);
		}
		temp = resize (temp, count); //resize the length of the array temp to the exact length
		return temp;
	}
	 //method for finding the words repeat in the array
	public static void findRepeat(String[] data) {
		while (data.length>=2) {
			data=removeRepeat(data);
		}
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		final int SIZE = 5;
						
		//prompt the user for words and store them in an array
		String[] in = acquireInput(input, SIZE);
		input.close();
		
		//find the words repeat in the array
		findRepeat(in);
		
	}

}
