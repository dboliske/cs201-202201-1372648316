package exams.first;

import java.util.Scanner;

public class Arrays {
	
	//Question 4  for Arrays

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter 5 words");//ask the user to enter 5 words
		String[] words = new String[5];
		for (int i=0; i<5; i++) {
			//acquire the input
			System.out.print("Enter word "+(i+1)+": ");//tell the user to enter the word and the number of the word
			String enter = input.nextLine();//acquire the word
			words[i] = enter;//put the word into a array named words
		}
		
		input.close();
		
		String[] find = new String[words.length-1];//create a new array that has the length of the words's length minus 1
		String test="";//set a variable to test whether the word appear more than once
		int count=0;//set the counter for the new array
		for(int i=0; i<(words.length-1);i++) {
			test=words[i];//from the first value of the array to begin testing until to the second to last 
			boolean repeat = false;
			for(int j=i+1;j<words.length;j++) {
				//test whether the values after the tested value equal to the tested value 
				if(test.equals(words[j])){	
					repeat=true;
					break;
				}
			}
			//to find if the value that appear more than once has been in the new array
			boolean inFind = false;
			for(int num=0; num<find.length; num++) {
				if(test.equals(find[num])) {
					inFind=true;
				}
			}
			//if the value repeat and it is not in the new array, put in the new array and update the count for the new array
			if(repeat && !inFind) {
				find[count]=test;//put the value appear more than once to the new array
				count++;//when the new acquire a new value, the counter update
			}			
		}
		
		if(count>0) {
			System.out.println("Word appear more than once:");
		}
		for(int i=0; i<count; i++) {
			//print the new array
			System.out.print(find[i]+"  ");
		}
	}

}
