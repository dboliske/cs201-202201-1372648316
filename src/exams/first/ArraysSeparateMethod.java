package exams.first;

import java.util.Scanner;

public class ArraysSeparateMethod {
	
	public static String[] readInput(Scanner a, int size) {
		//give a size, ask the user enter words in the required times, and put the input in an array
		String[] strs = new String[size];
		System.out.println("Please input "+size+" words.");
		//ask the user to enter words and put the words in an array
		for(int i=0; i<size; i++) {
			System.out.print("Enter word "+(i+1)+" : ");
			strs[i] = a.nextLine();
		}
		return strs;//return the array as the result of the method
	}
	
	public static boolean checkExist(String[] array, String v1) {
		//check whether the given parameter v1 exists in the given parameter array		
		for (int i=0; i<array.length; i++) {
			if (v1.equals(array[i])){
			    return true;  //if exist the method return true
			}    
		}
		return false;//otherwise the method return false
	}
	
	public static String[] resize(String[] array, int size ) {
		//resize the array using the given size
		String[] temp = new String[size];
		int limit = size < array.length ? size : array.length;//find the really required length for the array
		//copy the value of array to a new array with correct length
		for(int i=0; i<limit; i++) {
			temp[i]=array[i];
		}
		return temp;
	}
	
	public static String[] findRepeat(String[] in) {		
		//find the word that have repeated in the array of in
		String[] temp = new String[in.length-1];//set an array temp to store the repeated word	
		int count=0;//set a counter to count the number of words stored in the array temp
		//check every word in the array in to find if it repeat
		for(int i=0; i<(in.length-1);i++) {
			String w=in[i];
			boolean Repeat = false;
			for(int j=i+1; j<in.length; j++) {
				if(w.equals(in[j])) {
					Repeat = true;
				}
			}
			//check the repeated word if existed in the array temp			
			//when the repeated word do not exist in the array temp, put it in the array temp
			if(Repeat && !checkExist(temp,w)) {
				temp[count]=w;
				count++;
			}		
		}
		//resize the length of the temp to its real length
		temp=resize(temp,count);
		return temp;		
	}
	
	public static void main(String[] args) {
		
		//prompt the user for the array of words
		Scanner input = new Scanner(System.in);
		final int num = 5;
		String[] words = new String[num];
		words=readInput(input, num);
		input.close();
		
		//put the value of the array that appear more than once into a new array
		String[] out=findRepeat(words);
		if(out.length>0) {
			System.out.println(out.length+" words appear more than once.");
		}else {
			System.out.println("No word appear more than once. ");
		}
		for(int i=0; i<out.length; i++) {
			System.out.println("Word "+(i+1)+" : "+out[i]);
		}

	}

}
