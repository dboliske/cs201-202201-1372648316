package exams.first;

import java.util.Scanner;

public class DataType {
	
	//Question 1  for Data Types

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an integer: ");//ask the user to enter a whole number
		int num = Integer.parseInt(input.nextLine());//acquire the input
		num = num + 65;
		char c = (char)num;//convert the integer to character
		System.out.println("The character: "+c); //print the result to the console
		input.close();

	}

}
