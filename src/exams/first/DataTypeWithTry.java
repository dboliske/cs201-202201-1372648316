package exams.first;


import java.util.Scanner;

public class DataTypeWithTry {
	
	//Question 1  for Data Types with try/catch block to deal with the input error

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int num=0;//initialize the variable before the try/catch block
		//loop until the user enter the valid input: an integer
		boolean ValidInput = false;
		while(!ValidInput) {
			try {
				System.out.print("Enter an integer: ");//ask the user to enter a whole number
				num = Integer.parseInt(input.nextLine());//acquire the input
				ValidInput = true;
			}catch (Exception e) {
				System.out.println("It is not an integer, invalid input. Please enter again.");
			}
		}
				
		num = num + 65;
		char c = (char)num;//convert the integer to character
		System.out.println("The character: "+c); //print the result to the console
		input.close();

	}

}
