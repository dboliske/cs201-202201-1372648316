package exams.first;

//Question 5  for Objects
public class Pet {
	//the attribute
	private String name;
	private int age;
	
	//the default constructor
	public Pet() {
		name = "Pet 1";
		age = 1;
	}
	
	//the non-default construct
	public Pet(String name, int age) {
		this.name = name;
		this.age=1;//if the value of age is invalid keep the default value
		setAge(age);//if the value of age is valid, pass it to the constructor
	}
	
	//mutator for the name
	public void setName(String name){
		this.name = name;
	}
	
	//mutator for the age
	public void setAge(int age) {
		if (age>0) {
			//Zero and negative number are not positive. But I think it is possible that age is zero. So I set age greater and equal to 0 at first
			this.age = age;
		}
	}
	
	//accessor for the name
	public String getName() {
		return name;
	}
	
	//accessor for the age
	public int getAge() {
		return age;
	}
	
	//equals method
	public boolean equals(Object obj) {
		if(!(obj instanceof Pet)) {
			//if the obj do not belong to the class of Pet, return false
			return false;			
		}else {
			Pet p=(Pet) obj;//type cast the obj to Pet class
			//if the value of name and age don't equal, return false
			if (!p.getName().equals(this.name)) {
				return false;
			}else if (p.getAge()!=this.age) {
				return false;
			}
		}
		
		return true;		
	}
	
	//toString method
	public String toString() {
		return this.name + " has " +this.age+" year old.";
	}

}
