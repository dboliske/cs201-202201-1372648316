package exams.first;

public class PetClient {

	public static void main(String[] args) {
		Pet p1 = new Pet();
		Pet p2 = new Pet("WriteCat", 2);
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p1.equals(p2));
		p2.setAge(-3);
		System.out.println(p2);
		p2.setAge(3);
		System.out.println(p2);
		

	}

}
