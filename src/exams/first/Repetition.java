package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		//Question 3  for Repetition
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an integer: ");//ask the user to enter an integer
		int num = Integer.parseInt(input.nextLine());//acquire the input and convert to integer
		int count1=0;//initialize a counter for printing the blank
		int count2=num;//initialize another counter to control the star's reduction
		for (int i=0; i<num;  i++) {
			for(int j1=0; j1<count1; j1++ ) {
				System.out.print("  ");
			}
			for(int j2=0; j2<count2; j2++) {
				System.out.print(" *");
			}
			count1++;
			count2--;
			System.out.println("");//control changing the row
		}
		input.close();
	}

}
