package exams.first;


import java.util.Scanner;

public class RepetitionWithTry {

	public static void main(String[] args) {
		//Question 3  for Repetition with try/catch block to deal with the non-integer input
		Scanner input = new Scanner(System.in);
		
		//initialize the variable before the try/catch block
		String enter = "";
		int num = 0;
		
		//acquire the valid input by a flag controlled loop
		boolean ValidInput = false;
		while(!ValidInput) {
			try {
				System.out.print("Enter an integer: ");//ask the user to enter an integer
				enter = input.nextLine();//acquire the input
				num = Integer.parseInt(enter);//convert the input to an integer
				ValidInput=true;
			}catch(Exception e) {
				System.out.println("invalid input. You have entered ' "+ enter+" '. Please enter again");//deal with the error and tell the user why he is wrong
			}
		}
				
		int count1=0;//initialize a counter for printing the blank
		int count2=num;//initialize another counter to control the star's reduction
		for (int i=0; i<num;  i++) {
			for(int j1=0; j1<count1; j1++ ) {
				System.out.print("  ");
			}
			for(int j2=0; j2<count2; j2++) {
				System.out.print(" *");
			}
			count1++;
			count2--;
			System.out.println("");//control changing the row
		}
		input.close();
	}

}
