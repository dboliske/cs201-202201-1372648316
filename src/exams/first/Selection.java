package exams.first;

import java.util.Scanner;

public class Selection {
	
	//Question 2  for Selection

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter an integer: ");//ask the user to enter a whole number
		int num = Integer.parseInt(input.nextLine());//acquire the input and convert to a integer
		if(num%2==0 && num%3==0) {
			//if the integer is divisible by 2 and 3, print foobar to the console
			System.out.println("foobar");		
		}else if(num%2==0 && num%3!=0) {
			//if the integer is divisible by only 2, print foo
			System.out.println("foo");
		}else if (num%2!=0 && num%3==0) {
			//if the integer is divisible by only 3, print bar 
			System.out.println("bar");
		}
		//divisible by either, print nothing
		input.close();
	}

}
