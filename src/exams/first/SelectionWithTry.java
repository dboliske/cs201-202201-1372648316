package exams.first;


import java.util.Scanner;

public class SelectionWithTry {
	
	//Question 2  for Selection with the try/catch block to handle the user input error

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num=0;//initialize the variable before the try/catch block
		boolean ValidInput=false;
		//loop until the user enter a valid input: integer
		while(!ValidInput) {
			try {
				System.out.print("Enter an integer: ");//ask the user to enter a whole number
				num = Integer.parseInt(input.nextLine());//acquire the input and convert to a integer
				ValidInput=true;
			}catch(Exception e) {
				System.out.println("invalid input. Please enter an integer again.");
			}
		}
						
		if(num%2==0 && num%3==0) {
			//if the integer is divisible by 2 and 3, print foobar to the console
			System.out.println("foobar");		
		}else if(num%2==0 && num%3!=0) {
			//if the integer is divisible by only 2, print foo
			System.out.println("foo");
		}else if (num%2!=0 && num%3==0) {
			//if the integer is divisible by only 3, print bar 
			System.out.println("bar");
		}
		//divisible by either, print nothing
		input.close();
	}

}
