package exams.second;

public abstract class Circle extends Polygon {
//The class Circle inherit the class Polygon and implement the abstract method of the class Polygon
	
	private double radius; //the radius of the circle
	
	public Circle() { //default constructor
		super();
		radius = 1.0;
	}
	
	public void setRadius(double radius) { //setter for radius
		if (radius>0) {
			this.radius = radius;
		}		
	}
		
	public double getRadius() {//getter for radius
		return radius;
	}
	
	public String toString() { //toString method
		String result = super.toString() + " is a circle with radius of "+radius +".\n";
		return result;
	}

	@Override
	public double area() { //method for computing the area of the circle
		double result = Math.PI *radius *radius;
		return result;
	}

	@Override
	public double perimeter() { //method for computing the circumference of the circle
		double result = 2.0 * Math.PI *radius;
		return result;
	}

}
