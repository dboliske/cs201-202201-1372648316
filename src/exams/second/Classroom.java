package exams.second;

public class Classroom {
//The class Classroom	
	
	protected String building;  //the building where the classroom locate
	protected String roomNumber; //the Number of the room where the classroom is in
	private int seats; //the seats that the classroom has
	
	public Classroom() { //default constructor
		building = "Building X";
		roomNumber = "001";
		seats = 30;		
	}
	
	public void setBuilding(String building) { //setter for building 
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) { //setter for roomNumber
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) { //setter for seats
		if(seats>0) {
			this.seats = seats;
		}		
	}


	public String getBuilding() { //getter for building
		return building;
	}

	
	public String getRoomNumber() { //getter for roomNumber
		return roomNumber;
	}

	public int getSeats() { //getter for seats
		return seats;
	}
	
	public String toString() { //toString method
		String result = "The classroom's information:\n";
		result += "Building: "+ building +"\n";
		result += "Room Number: " + roomNumber + "\n";
		result += "Seats: "+seats + "\n";
		return result;
	}
	
	

	

	
	
	

}
