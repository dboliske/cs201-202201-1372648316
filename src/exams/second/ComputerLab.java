package exams.second;

public class ComputerLab extends Classroom {
//The class ComputerLab inherit the class Classroom
	
	private boolean computers; //represent whether the computer lab has computer
	
	ComputerLab(){ //default constructor
		super();
		computers = true;
	}
	
	public void setComputers(boolean computers) { //setter for computers
		this.computers = computers;
	}

	public boolean hasComputers() { //getter foe computers
		return computers;
	}

	@Override
	public String toString() { //toString method
		String result = super.toString();
		result += "Has computers:" + ((hasComputers())? "Yes":"No");
		return result;
	}
	
	

}
