# Final Exam

## Total

95/100

## Break Down

1. Inheritance/Polymorphism:    20/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  5/5
2. Abstract Classes:            18/20
    - Superclass:               5/5
    - Subclasses:               3/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  17/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    2/5
    - Results:                  5/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        20/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  5/5

## Comments

1. ok
2. Subclass circle is not an abstract class. -2
3. The program didn't use "done" as an exit condition. -3
4. ok
5. ok