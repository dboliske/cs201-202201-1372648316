package exams.second;

public abstract class Polygon {
//The abstract class Polygon
	
	protected String name; //the name of the polygon
	
	public Polygon() { //default constructor
		name = "Polygon1";
	}

	public void setName(String name) { //setter for name
		this.name = name;
	}
	
	public String getName() { // getter for name
		return name;
	}

	public String toString() { //toString method
		return "Polygon: "+ name;
	}
	
	public abstract double area(); //abstract method for computing the area of the polygon
	
	public abstract double perimeter();	//abstract method for computing the circumference of the polygon

}
