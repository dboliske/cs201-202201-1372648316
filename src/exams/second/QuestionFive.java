package exams.second;

import java.util.Scanner;

public class QuestionFive {
//solution for Question Five: Searching
	
	public static int sequentialSearch(double[] arrays, double value, int start, int end) {
	//search from the value index at start to the value index at end of the array using the sequential search
		for (int i=start; i<=end && i<arrays.length; i++) {
			if (arrays[i]==value) {
				return i;
			}
		}
		return -1;
	}
	
	public static int jumpSearch(double[] arrays, double value, int prev, int step) {
	//search the value from the value index at prev of the array using the jump search algorithm, the jump search is implemented recursively 
		int result = -1;		
		
		int end = prev + step;
		
		if(arrays[prev]> value) {
			result = -1;
		} else if(arrays[arrays.length-1]<value) {
			result = -1;
		} else if(arrays[Math.min(arrays.length-1, end)]<value) {
			result = jumpSearch(arrays, value, end, step);	
		} else {
			result = sequentialSearch(arrays, value, prev, end);		
		}
		
		return result;
	}

	public static void main(String[] args) {
		double[] numberArrays = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.print("The value looked for: ");
		
		boolean correct = false;
		double value = 0.0;
		do {
			try {
				String in = input.nextLine();
				value = Double.parseDouble(in);
				correct = true;
				break;
			} catch(Exception e) {
				System.out.println("Unvailable input. Please enter again.");
			}			
		} while (!correct);
		
		input.close();
		
		int step = (int) Math.sqrt(numberArrays.length);
		
		int index = jumpSearch(numberArrays, value, 0, step);
		System.out.println("The position of the value in the array: " + index);			

	}

}
