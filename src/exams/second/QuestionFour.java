package exams.second;

public class QuestionFour {
//solution for Question Four: Sorting

	public static void main(String[] args) {
		
		String[] arrays = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource","lake", "importance"};
		
		//find the minimum for the array at i from start to the last second
		for(int i=0; i<arrays.length-1; i++) {
			//find the minimum of the arrays between the ith value to the last value of the array
			int min = i;
			for (int j=i+1; j<arrays.length; j++) {
				if (arrays[j].compareTo(arrays[min])<0 ) {
					min = j;
				}
			}
			if(min!=i) {
				String temp = arrays[i];
				arrays[i] = arrays[min];
				arrays[min] = temp;
			}
		}
		//print the sorted array
		for (String a: arrays) {
			System.out.print(a+"  ");
		}

	}

}
