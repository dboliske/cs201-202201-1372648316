package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThree {
//solution for Question Three: ArrayLists

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Double> numberList = new ArrayList<Double>();
		String enter;
		double number;
		
		//ask the user to enter a sequence of numbers until they enter "exit"
		boolean done = false;
		while (!done) {
			System.out.print("Enter a number(enter 'exit' to leave): ");
			enter = input.nextLine();
			if (enter.toLowerCase().equals("exit")){
				done = true;
				break;
			}
						
			int count;
			try {
				count = numberList.size();
				number = Double.parseDouble(enter);
				//if it is the first number, add directly
				if (count==0) {
					numberList.add(number);
				} else {
				//if it is not the first number, use insertion sort to find the correct location for it
					numberList.add(0.0);
					for (int i= count-1; i>=0; i--) {
						if (numberList.get(i)>number) {
							count--;
							numberList.set(i+1,numberList.get(i));
						}					
					}
					numberList.set(count, number);	
				}			
				
			} catch (Exception e) {
				System.out.println("unavailable input. Please enter again.");
			}
		}
		input.close();
		
		if(numberList.size()==0) {
			System.out.println("Sorry, you haven't entered the number. There is no number.");
		} else {
			//print the maximum
			System.out.println("The maximum value of the ArrayList: " + numberList.get(numberList.size()-1));
			//print the minimum
			System.out.println("The minimum value of the ArrayList: " + numberList.get(0));
		}
	}
}
