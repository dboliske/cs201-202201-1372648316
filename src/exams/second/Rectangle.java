package exams.second;

public class Rectangle extends Polygon {
//The class Rectangle inherit the class Polygon	and implement the abstract method of the class Polygon
	
	private double width;  // the width of the rectangle
	private double height; //the height of the rectangle
	
	public Rectangle() { //default constructor
		super();
		width = 1.0;
		height = 1.0;
	}
	
	public void setWidth(double width) { //setter for width
		if(width>0) {
			this.width = width;
		}		
	}
	
	public void setHeight(double height) { //setter for height
		if (height>0) {
			this.height = height;
		}		
	}

	public double getWidth() { //getter for width
		return width;
	}

	public double getHeight() { //getter for height
		return height;
	}
	
	public String toString() { //toString method
		String result = super.toString() + " is a circle with width of "+width + " and height of " + height +".\n";
		return result;
	}

	@Override
	public double area() { //method for computing the area of the rectangle
		double result = height*width;
		return result;
	}

	@Override
	public double perimeter() { //method for computing the circumference of the rectangle
		double result = 2.0 * (height + width);
		return result;
	}

}
