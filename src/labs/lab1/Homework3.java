package labs.lab1;

import java.util.Scanner;

public class Homework3 {

	// ask a name and print the initial of the name to console 
	public static void main(String[] args) {
		System.out.print("Enter your first name: "); //ask for name
		Scanner input = new Scanner(System.in); //assign the user's input to a variable
		char c= input.nextLine().charAt(0); //get the initial of name
		input.close();
		
		System.out.println("Your name's initial: "+c);

	}

}
