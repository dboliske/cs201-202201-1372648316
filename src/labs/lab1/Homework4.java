package labs.lab1;

//I have test the program with the test table as following 
//Fahrenheit 20 35 60 65 l86 90
//Celsius -5 4 16 18 30 32
//the result is as my expectation
//I have modify the program to Test4 to perform the test
//the test result is recorded in testtable4

import java.util.Scanner;

public class Homework4 {

	public static void main(String[] args) {
	Scanner input = new Scanner (System.in);
	//ask a temperature in Fahrenheit and convert it  to Celsius
	System.out.print("Enter a temperature in Fahrenheit: ");//ask a temperature in Fahrenheit 
	double f = Double.parseDouble(input.nextLine());
	double c=(f-32)*5/9;//convert Fahrenheit to Celsius
	System.out.println("temperature in Celsius: "+c);
	System.out.println("");
	
	//ask a temperature in Celsius and convert to Fahrenheit 
	System.out.print("Enter a temperature in Celsius: ");//ask a temperature in Celsius
	double c1= Double.parseDouble(input.nextLine());
	double f1=(c1*9/5)+32;//convert Celsius to Fahrenheit
	System.out.println("temperature in Fahrenheit: "+f1);
	input.close();
	
	}

	}
