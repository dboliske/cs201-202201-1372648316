package labs.lab1;

//I have test the program with different numbers
//The result is as my expectation
//I have modify the program to Test5 to record the test record in five number once
//I have recorded some test result in the testtable5

import java.util.Scanner;

public class Homework5 {

	//calculate the amount of wood for a box
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.print("Enter the length of a box in feet: ");//ask the length
		int l =Integer.parseInt(input.nextLine());
		System.out.print("Then enter the width of the box in feet: ");//ask the width
		int w = Integer.parseInt(input.nextLine());
		System.out.print("Lastly, enter the depth of the box: ");//ask the depth
		int d =Integer.parseInt(input.nextLine());
		System.out.println("The amount of wood (square feet) needed to make the box: "+((l*w+l*d+w*d)*2));//calculate and print the amount of wood needed to the console
		input.close();
		
	}

}
