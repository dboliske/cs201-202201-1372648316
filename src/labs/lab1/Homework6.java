package labs.lab1;

import java.util.Scanner;

public class Homework6 {
		
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] inch=new double[10];
		double[] cent=new double[10];
		for(int i=0; i<10; i++) {
			System.out.print("Enter a number in inches: ");//ask for an inch
		    double in = Double.parseDouble(input.nextLine());
		    inch[i]=in;
		    System.out.println("the centimeters of the number: "+(in*2.54));
		    cent[i]=in*2.54;
		}

	    input.close();
	    System.out.println("");
	    
	    for(int j=0; j<10; j++) {
	    	System.out.println("inches:"+inch[j]+"    centimeter:"+cent[j]);
	    }
	}

}
