package labs.lab1;

import java.util.Scanner;

public class Test4 {

	public static void main(String[] args) {
	Scanner input = new Scanner (System.in);

    double[] fah1 = new double[6];
    double[] cel1 = new double[6];
    double[] fah2 = new double[6];
    double[] cel2 = new double[6];
    		
    for (int i=0; i<6; i++) {		
    	  System.out.print("Enter a temperature in Fahrenheit: ");//ask a temperature in Fahrenheit 
	      double f = Double.parseDouble(input.nextLine());
	      fah1[i] = f;
	      double c=(f-32)*5/9;//convert Fahrenheit to Celsius
	      System.out.println("temperature in Celsius: "+c);
	      cel1[i] = c;
	      System.out.println("");
	
	      System.out.print("Enter a temperature in Celsius: ");//ask a temperature in Celsius
	      double c1= Double.parseDouble(input.nextLine());
	      cel2[i]=c1;
	      double f1=(c1*9/5)+32;//convert Celsius to Fahrenheit
	      fah2[i]=f1;
	      System.out.println("temperature in Fahrenheit: "+f1);
	      System.out.println("");
	      
	}
    input.close();
    System.out.println("");
    for (int j1=0; j1<6; j1++) {
    	System.out.print(fah1[j1]+" , ");
    }
    System.out.println("");
    for (int j2=0; j2<6; j2++) {
        System.out.print(cel1[j2]+" , ");	
    }
    
    System.out.println("");
    for (int k1=0; k1<6; k1++) {
    	System.out.print(cel2[k1]+" , ");                          
    }
    System.out.println("");
    for(int k2=0; k2<6; k2++) {
    	System.out.print(fah2[k2]+" , ");
    }
    
	}
}
