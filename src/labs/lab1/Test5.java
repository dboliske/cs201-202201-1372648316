package labs.lab1;

import java.util.Scanner;

public class Test5 {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		double[] len= new double[5];
		double[] wid=new double[5];
		double[] dep=new double[5];
		double[] amount=new double[5];
		
		for(int i=0; i<5;i++) {
			System.out.print("Enter the length of a box in feet: ");//ask the length
			int l =Integer.parseInt(input.nextLine());
			len[i]=l;
			System.out.print("Then enter the width of the box in feet: ");//ask the width
			int w = Integer.parseInt(input.nextLine());
			wid[i]=w;
			System.out.print("Lastly, enter the depth of the box: ");//ask the depth
			int d =Integer.parseInt(input.nextLine());
			dep[i]=d;
			System.out.println("The amount of wood (square feet) needed to make the box: "+((l*w+l*d+w*d)*2));
			System.out.println("");
			amount[i]=(l*w+l*d+w*d)*2;
		}
		
		input.close();
		
		for (int j=0; j<5; j++) {
			System.out.println("length: "+len[j]+"   width:"+wid[j]+"   depth: "+dep[j]+"   the amount of wood needed: "+amount[j]);
		}
	}

}
