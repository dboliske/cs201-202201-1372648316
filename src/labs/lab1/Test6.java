package labs.lab1;
//I have tested the program 
//The result is as my expectation
//I have modify the program to record the test result in 10 number
//The testtable6 is the test result in some a time

import java.util.Scanner;

public class Test6 {

	//convert inches to centimeters
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a number in inches: ");//ask for an inch
	    double in = Double.parseDouble(input.nextLine());
	    System.out.println("the centimeters of the number: "+(in*2.54));//convert and print the centimeter of the inch to console

	    input.close();
	}

}
