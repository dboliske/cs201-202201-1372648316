package labs.lab2;

import java.util.Scanner;

public class Question2 {
	
	//ask no fixed grades from the user and calculate the average of the grades  

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 0;
		double sum = 0;
		double grade = 0;
		//ask the user for the grade in infinite times only exists when entering the negative number 
		 while (grade>=0) {
			System.out.print("grade the exam(enter a negetive number to exist): ");//ask the user to grade 
			grade = Double.parseDouble(input.nextLine());//assign the grade to a variable
			if (grade<0) {
				break;
			}
			sum = sum + grade;
			number++;
		}
        
		 input.close();
		 System.out.println("the average of the exam: "+(sum/number));//calculate and print the average of grades to the console
	}

}
