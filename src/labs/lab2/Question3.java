package labs.lab2;

import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		while(true) {
			
			//repeatedly display a menu of choices to the user
			System.out.println("option 1: Say Hello");
			System.out.println("option 2: Addition");
			System.out.println("option 3: Multiplication");
			System.out.println("option 4: Exit");
			
			//prompt the user to enter an option
			System.out.print("Please enter your choice(the number of the option):");
			int c = Integer.parseInt(input.nextLine());//assign the user's choice to a variable
			System.out.println("");
			//perform the different execution according to the user's choice
			switch(c){
				//option 1   print hello to the console
			    case 1: 
					System.out.println("Hello");//print hello to console
					System.out.println("");
				    break;
				//option 2  prompt use to enter 2 numbers and return the sum of them
				case 2:
					System.out.print("X=");//ask for a number 
					double num1 = Double.parseDouble(input.nextLine());
					System.out.print("Y=");//ask for another number
					double num2 = Double.parseDouble(input.nextLine());
					System.out.println("X+Y="+(num1+num2));//print the sum of the two numbers to the console
					System.out.println("");
				    break;
				//option 3  prompt use to enter 2 numbers and return the produce of them 
				case 3:
					System.out.print("X=");//ask for a number
					double num3 = Double.parseDouble(input.nextLine());
					System.out.print("Y=");//ask for another number
					double num4 = Double.parseDouble(input.nextLine());
					System.out.println("X*Y="+(num3*num4));//print the product of the two numbers to the console
					System.out.println("");
				    break;
				//option 4 leave the loop first
				case 4:	
					break;
				default:
					System.out.println("I'm sorry! But we have not the option.");
					System.out.println("");
				    break;
						}
			
			if(c==4) {
				//when option 4 leave the program
				break;
							}
			
		}
		input.close();
		System.out.println("Goodbye!");

	}

}
