//Min Wei for lab3's question 1
package labs.lab3;

import java.io.File;
import java.util.Scanner;
import java.io.IOException;

public class Question1 {

	public static void main(String[] args) throws IOException {
		File file= new File("src/labs/lab3/grades.csv");  //construct  a new file object
		Scanner input= new Scanner(file); //construct a new Scanner object to read the file
		int counter=0;
		double[] grades= new double[14];
		while(input.hasNextLine()) {
			//  use end-of-file loop to assign the grades in the file to the array grades
			String[] value= input.nextLine().split(",");//read the line that user enter and split it by the comma
			grades[counter]= Double.parseDouble(value[1]); //assign the value of the array value at 1 after converted to double to the array grades at counter  
			counter++;					 
		}
		double total=0;
		for(int i=0;i<grades.length; i++) {
			 //calculate the sum of the grades
			  total=total+grades[i];
		}
	input.close();
	System.out.println("Average grade:"+(total/grades.length));		//print the average of the grades to the console
	}

}
