// Min Wei  lab3'question2 2022/2/12
package labs.lab3;

import java.io.FileWriter;
import java.util.Scanner;

public class Question2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean done=false;//initializing a flag variable for a do loop
		double[] numbers= new double[5];//declare and initialize a array to store the input from the user
		int counter=0;//set a counter to count the number of the input
		
		do {
			//prompt user to enter numbers until "done" and store the inputs to the array numbers
			//add the try catch block to handle the possible error 
		  try {
			  System.out.print("Enter a number( enter done to exit):");	//prompt the user to enter a number
		      String str=input.nextLine();//get the user's input and assign it to the variable str
		      if (str.equals("done")) {
		    	  //when the user enter "done", change the flag variable to finish the input
			       done=true;
			       break;
		      }
		      if (counter==numbers.length) {
		    	  //resize the array to store the input
		    	  double[] bigger = new double[numbers.length*2];//create a new array with the longer length
			      for (int i=0; i<numbers.length; i++) {
			    	  //copy the value of the array numbers to the new array
				     bigger[i]=numbers[i];
			      }
			      numbers=bigger;//point the array numbers to the new array's reference
			      bigger=null;	//point the new array to null 		
		      }
		      numbers[counter]=Double.parseDouble(str);//convert the input to double and store them to the array numbers
		      counter++;	
		   }catch(Exception e) {
			   //when error, tell the user what's wrong and what shall he do
			  System.out.println(e.getMessage());
			  System.out.println("Please enter a valid number or done");
		   }
		  
		} while(!done);
		
		//trim the array to delete the useless data
		double[] smaller= new double[counter];//create a new array that has the exact length to contain the input
		for(int i=0; i<counter; i++) {
			//copy the value of the array numbers to the new array
			smaller[i]=numbers[i];
		}
		numbers=smaller;//point the array numbers to the new array's reference
		smaller=null;//point the new array to null
		
		//write the array to a file and use the try catch block to protect from the error
		//and ask the user for the name of the file
		try {
		   System.out.print("Please a file name:");//ask the user for the name of the file
		   String name = input.nextLine();
		   FileWriter file= new FileWriter("src/labs/lab3/"+name);//create the FileWriter object 
		   String number="";
		   for(int i=0;i<counter; i++) {
		   number=number+numbers[i]+"  ";  //convert the number in the array to String
		   }
		   file.write(number);//write the content to the file
		   file.flush();
		   file.close();		   
		}catch(Exception e) {
			System.out.println(e.getMessage());				
		  }
		
       
		 input.close();
         System.out.println("finished filewriter");
        
        }
	
}

