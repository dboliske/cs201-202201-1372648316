package labs.lab4;

public class Geolocation {

	protected double lat;
	protected double lng;
	
	public Geolocation() {
		lat = 0;
		lng = 0;
	}
	
	public Geolocation(double lat, double lng) {
		this.lat = 0;
		setLat(lat);
		this.lng = 0;
		setLng(lng);
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) {
		if (validLat(lat)) {
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) {
		if(validLng(lng)) {
			this.lng = lng;
		}
	}
	
	public boolean validLat(double lat) {
		if (lat>=90 || lat<=-90) {
			return false;
		}
		return true;
	}
		
	public boolean validLng(double lng) {
		if(lng>=180 || lng<=-180) {
			return false;
		}
		return true;
	}
	
	public boolean equals(Geolocation g) {
		if (this.lat!= g.getLat()) {
			return false;
		} else if (this.lng!= g.getLng()) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return " (" +lat + ","+lng+ ")";
	}
	
	public double calcDistance(Geolocation g) {
		return Math.sqrt(Math.pow((this.lat-getLat()),2)+Math.pow((this.lng-g.getLng()), 2));			
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow((this.lat-lat), 2)+Math.pow((this.lng-lng), 2));
	}
			
}
