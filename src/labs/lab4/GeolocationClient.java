package labs.lab4;

public class GeolocationClient {

	public static void main(String[] args) {
		Geolocation g1 = new Geolocation();
		Geolocation g2 = new Geolocation(30, 60) ;
		
		System.out.println("( "+g1.getLat()+" , "+g1.getLng()+" )");
		System.out.println("( "+g2.getLat()+" , "+g2.getLng()+" )");

	}

}
