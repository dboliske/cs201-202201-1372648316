# Lab 4

## Total

25/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         3/3

## Comments

Good, but you validation methods do not follow the UML diagram and your Potion class does not validate before setting strength in either the non-default constructor nor the mutator.
