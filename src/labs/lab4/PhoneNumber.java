package labs.lab4;

public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "0086";
		areaCode = "010";
		number = "8888888";		
	}
	
	public PhoneNumber (String cCode, String aCode, String number) {
		countryCode = cCode;
		areaCode = "010";
		setAreaCode (aCode);
		this.number = "8888888";
		setNumber(number);
	}

	public String getCountryCode(){
		return countryCode;		
	}
	
	public String getAreaCode() {
		return areaCode;		
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String cCode) {
		countryCode = cCode;
	}
	
	public void setAreaCode(String aCode) {
		if (validAreaCode(aCode)) {
			areaCode = aCode;
		}
	}
	
	public void setNumber(String number) {
		if (validNumber(number)) {
			this.number = number;
		}
	}
	
	public String toString() {
		return countryCode + "-" + areaCode + "-" +number;
	}
	
	public boolean validAreaCode(String aCode) {
		if (aCode.length()==3) {
			return true;			
		}
		return false;		
	}
	
	public boolean validNumber(String number) {
		if (number.length()==7) {
			return true;
		}
		return false;
	}
	
	public boolean equals (PhoneNumber pn) {
		if (this.countryCode != pn.getCountryCode() || this.areaCode != pn.getAreaCode() || this.number != pn.getNumber()) {
			return false;
		}
		return true;
	}
}
