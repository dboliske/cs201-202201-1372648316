package labs.lab4;

public class Potion {
	private String name;
	private double strength;
	
	public Potion(){
		name = "Potion 1";
		strength = 5;
	}
	
	public Potion(String name, double strength) {
		this.name = name;
		this.strength = strength;
	}

	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		if(validStrength(strength)) {
			this.strength = strength;
		}
	}
	
	public String toString() {
		return name + " has a strength of "+ strength;
	}
	
	public boolean validStrength(double strength) {
		if (strength <=10 && strength >=0) {
			return true;
		}
		return false;			
	}
	
	public boolean equals(Potion p) {
		if(this.name!=p.getName()) {
			return false;
		} else if (this.strength != p.strength) {
			return false;
		}
		return true;
	}
}
