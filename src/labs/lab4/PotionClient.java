package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion();
		Potion p2 = new Potion("herb", 6);
		
		System.out.println(p1.toString());
		System.out.println(p2.toString());
	}

}
