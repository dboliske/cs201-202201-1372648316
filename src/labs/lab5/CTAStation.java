package labs.lab5;

import labs.lab4.Geolocation;

public class CTAStation extends Geolocation {

	    //set attributing
		private String name;
		private String location;
		private boolean wheelchair;
		private boolean open;
		
		//default constructor
		public CTAStation() {
			super();
			name = "Station 1";
			location = "Someplace";
			wheelchair = true;
			open = true;
		}
		
		//non-default constructor
		public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
			super(lat, lng);
			this.name = name;
			this.location = location;
			this.wheelchair = wheelchair;
			this.open = open;		
		}

		//accessor for name
		public String getName() {
			return name;
		}

		//accessor for location
		public String getLocation() {
			return location;
		}

		//accessor for wheelchair
		public boolean hasWheelchair() {
			return wheelchair;
		}

		//accessor for open
		public boolean isOpen() {
			return open;
		}

		//mutator for name
		public void setName(String name) {
			this.name = name;
		}

		//mutator for location
		public void setLocation(String location) {
			this.location = location;
		}

		//mutator for wheelchair
		public void setWheelchair(boolean wheelchair) {
			this.wheelchair = wheelchair;
		}

		//mutator for open
		public void setOpen(boolean open) {
			this.open = open;
		}
		
		//toString method
		public String toString() {
			return name + "," + lat + "," + lng + ","  + location + "," +  wheelchair + "," + 	open;
		}
		
		//equals method
		public boolean equals(CTAStation cs) {
			if (!super.equals(cs)) {
				return false;
			}
			if (this.name!=cs.getName()) {
				return false;
			}else if (this.location!=cs.getLocation()) {
				return false;
			}else if (this.wheelchair!=hasWheelchair()) {
				return false;
			}else if (this.open!=isOpen()) {
				return false;
			}
			return true;
		}

}
