package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
	
	//method for resizing the length of array to the required length
	public static CTAStation[]  resize(CTAStation[] array, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = array.length < size? array.length: size;
		for (int i=0; i<limit; i++) {
			if (array[i]!=null) {
				temp[i] = array[i];				
			}
		}
		return temp;		
	
	}
    //read the data from the file that the user appoint and store the data in an array
	public static CTAStation[] readFile(String filename) {
		CTAStation[] cs = new CTAStation[10];//initialize the array
		//use the try/catch block to handle the error in reading the file
		try {
			File f = new File(filename);
			Scanner in = new Scanner(f);
			int count = 0;//initialize a counter for the array to store data
			//use the try/catch block to deal with the error in reading every line
			try {
				//end-of-file loop
				while(in.hasNextLine()) {
					String inputData = in.nextLine();
					if (count == cs.length) {
						//resize the array to a new longer length
						cs = resize(cs, cs.length*2);
					}
					String[] value = inputData.split(",");
					CTAStation val = null;
					try { 
						//read every line of data and type cast them to the correct type
						val =
						new CTAStation (value[0],
								Double.parseDouble(value[1]),
								Double.parseDouble(value[2]),
								value[3],
								Boolean.parseBoolean(value[4]),
								Boolean.parseBoolean(value[5]));
					}catch(Exception e) {
						//Error in reading data and type cast them to the correct type
					}
					if (val!=null) {
						//if there is data that have been read in, store the data in array cs and update the counter for the array
						cs[count] = val;
						count++;
					}					
			     }			    		
			     }catch(Exception e) {
				 //Error in reading in a line			     
			     }
				 in.close();
				 cs=resize(cs,count);	//delete the blank position in the array
		      }catch(FileNotFoundException fnf) {
		    	  cs = null; //There is no file
		      }catch(Exception e) {
			      System.out.println("Error in reading in file.");			      
		      }		
		      return cs;				
	}
	
	//method for displaying the station's name
	public static void displayStationNames(CTAStation[] data) {
		System.out.println("The Station Name list as following:");
		for (int i=0; i<data.length; i++) {
			System.out.println("Station "+(i+1)+":"+data[i].getName());
		}
	}
	
	//method for displaying the station with wheelchair access or without wheelchair access
	public static void displayByWheelchair(CTAStation[] data, Scanner input) {		
		System.out.println("Do you want to have accessibility ?");
		boolean done = false;
		String in ="";
		//loop that prompt the user for yes or no until the right enter
		while (!done) {
			System.out.print("Accessibility (y or n)  ");
			in = input.nextLine();
			if (in.equalsIgnoreCase("y") ||in.equalsIgnoreCase("yes") || in.equalsIgnoreCase("n")||in.equalsIgnoreCase("no")) {//if user enter valid input exit the loop
				done = true;				
			}			
		}
		
		switch(in.toLowerCase()) {
		   //when the user enter yes or y, list the station with wheelchair
		   case "y":
		   case "yes":
			   CTAStation[] hasWheel = new CTAStation[data.length];
			   int count1 = 0;
			   //find stations that have wheelchair access
			   for (int i=0; i<data.length; i++) {
				   if (data[i].hasWheelchair()) {
					   hasWheel[count1]=data[i];
					   count1++;
				   }
			   }
			   hasWheel = resize(hasWheel, count1);
			   if(count1<=0) {//if there is no stations that have wheelchair access tell the user
				   System.out.println("No stations that meet the requirement are found. ");
			   }else {//When there are stations that have wheelchair access, print the station's name
				   System.out.println("The Station List with the Wheelchair Access as following: ");
				   for (int j=0; j<count1; j++) {
					   System.out.println("Station "+(j+1)+" :"+hasWheel[j].getName());
				   }
			   }
			   break;
			   
           //when the user enter no or n, print the station without wheelchair
           case "n":
		   case "no":
			   CTAStation[] noWheel = new CTAStation[data.length];
			   int count2 = 0;
			   //find stations that have not wheelchair access
			   for (int i=0; i<data.length; i++) {
				   if (!data[i].hasWheelchair()) {
					   noWheel[count2]=data[i];
					   count2++;
				   }
			   }
			   noWheel = resize(noWheel, count2);
			   
			   if(count2<=0) {//if there is no stations that have not wheelchair access, tell the user
				   System.out.println("No stations that meet the requirement are found. ");
			   }else {//when there are stations that have no wheelchair access, print the names of the stations
				   System.out.println("The Station List without the Wheelchair Access as following: ");
				   for (int j=0; j<count2; j++) {
					   System.out.println("Station "+(j+1)+" : "+noWheel[j].getName());
				   }
			   }
			   break;		  
		 }		
	}
	
	//method for display the station that is nearest to the inputed latitude and longitude 
	public static void displayNearest(CTAStation[] data, Scanner input) {
		System.out.println("Please enter the latitude and longitude.");
		System.out.print("latitude: ");
		double lat = Double.parseDouble(input.nextLine());
		System.out.print("longtitude: ");
		double lng = Double.parseDouble(input.nextLine());
		int m = 0;
		//find the index for the nearest station
		for(int i=1; i<data.length; i++) {
			if (data[i].calcDistance(lat,lng)<data[m].calcDistance(lat,lng)) {
				m=i;
			}
		}
		//print the nearest station's name and latitude and longitude
		System.out.println("the nearest station to the place: Station "+ data[m].getName()+
				" at the latitude "+data[m].getLat()+" longtitude "+data[m].getLng());
	}
	
	//method for menu
	public static void menu(CTAStation[] data) {
		boolean done = false;
		
		//loop for the choice until the user choose "4" to exit		
		do {
			Scanner input = new Scanner (System.in);
			System.out.println("1.Display Station Names");
			System.out.println("2.Display Station with/without Wheelchair access");
			System.out.println("3.Display Nearest Station");
			System.out.println("4.Exit");
			System.out.print("Choice:");
			String choice = input.nextLine();			
					
			switch(choice) {
			    case "1":
				  //Display Station Names
				   displayStationNames(data);
				   break;
				case "2":
					//Display Stations with/without Wheelchair access
					displayByWheelchair(data,input);
					break;
				case "3":
					//Display Nearest Station
					displayNearest(data, input);
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("I don't understand that, and I can't do that.")	;	
		     }
			 System.out.println("");
			
	      } while(!done);	
	}	
    	
	
	
	public static void main(String[] args) {
	     
		CTAStation[] cs;
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a file name: ");
		String filename = input.nextLine();//ask the filename from the user
		
		//calls readFile to load the data and get the data to pass to menu
		cs = readFile(filename);
		
		// menu
		try {
		//to handle the error of NullPointerException
			if (!cs.equals(null)) {
				menu(cs);
			}
		} catch(NullPointerException np) {
		System.out.println("There is no data available.") ;
		}		
		input.close();
		System.out.println("The programming has finished. Thank you!");
	}
 }
		


