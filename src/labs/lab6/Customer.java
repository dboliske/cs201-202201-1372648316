package labs.lab6;

import java.util.Date;

public class Customer {
	/* initialize the attribute*/
	private String name;
	private int queueNumber;
	private Date date; //set for writing reports
	
	//default constructor
	public Customer() {
		name = "Customer X";
		date = new Date();
		queueNumber = 0;		
	}
	
	//non-default constructor
	public Customer(String name) {
		this.name = name;
		this.queueNumber = 0;
		date = new Date();
	}
	
	
	//non-default constructor
	public Customer(String name, int queueNo) {
		this.name = name;		
		this.queueNumber = 0;		
		setQueueNumber(queueNo);
		this.date = new Date();
	}

	//Accessor for name
	public String getName() {
		return name;
	}
    //Mutator for name
	public void setName(String name) {
		this.name = name;
	}

	//Accessor for date
	public Date getDate() {
		return date;
	}
    
	//Mutator for date
	public void setDate(Date date) {
		this.date = date;
	}

	//Accessor for queueNumber
	public int getQueueNumber() {
		return this.queueNumber;
	}

	//Mutator for queueNumber
	public void setQueueNumber(int queueNumber) {
		if (queueNumber>=0) {
			this.queueNumber = queueNumber;
		}
    }
	
	//equals method
	public boolean equals(Object obj) {
		if(!(obj instanceof Customer)) {
			return false;
		}
		Customer c = (Customer) obj;
		if (this.name!=c.getName()||this.queueNumber!=c.getQueueNumber()) {
			return false;
		}
		return true;
	}
	
	//toString method	
	public String toString() {
		return "Dear "+name+" , your queue number is "+queueNumber;
	}
	
	//method for storing the data in csv format
	public String toCSV() {
		return queueNumber+","+name+","+date;
	}

}
