package labs.lab6;

import java.util.Scanner;
import java.io.FileWriter;
import java.util.ArrayList;

public class DeliApplication {	
	
	static ArrayList<Customer> customerList = new ArrayList<Customer>();
	static ArrayList<Customer> toReport = new ArrayList<Customer>(20);
	
	/*Method for adding the customer to the queue.*/
	public static ArrayList<Customer> addCustomer(Scanner input) {
		//loop to add the customer to the queue until entering "exit" to exit loop to return to menu
		boolean exitAcquireQueueNumber = false;
		do {
			System.out.println("Acquire Queue Number here. Enter exit to exit acquiring number.");
			System.out.print("Name:");
			String name = input.nextLine();
			if (name.equalsIgnoreCase("exit")){
				exitAcquireQueueNumber = true;
				break;
			}
			int num=1;//set the first customer's queue number as 1
			if (customerList.size()>0) {
				num=customerList.get(customerList.size()-1).getQueueNumber()+1;	//use the last customer's queue number plus 1 as the queue number
			}			
			Customer c = new Customer(name, num);			
			customerList.add(c);
			System.out.println(c+ ". There are "+ customerList.indexOf(c)+" customers before you. \n"
					+ "Please wait. We will service you as soon as we can.");//print out the queue number for the customer
			System.out.println("");
		}while(!exitAcquireQueueNumber);		
		return customerList;
	}
	
	/*Method for remove the customer from the queue*/
	public static ArrayList<Customer> helpCustomer(ArrayList<Customer> list){
		System.out.println("Queue number "+customerList.get(0).getQueueNumber()+
				" , Customer "+customerList.get(0).getName()+" , it is your turn.");//service the customer
		toReport.add(customerList.get(0));//add the serviced customer to the report
		customerList.remove(0);//remove the serviced from the queue
		System.out.println("");
		return customerList;
	}
	
	/*Method for menu*/
	public static ArrayList<Customer> menu(Scanner input){
		
		String[] options = {"Add customer to queue", "Help customer","Exit"};
		//loop the menu until enter option 3 to exit
		boolean done = false;
		do{
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1)+"."+options[i]);
			}
			System.out.print("Choice:");
			String choice = input.nextLine();
			switch(choice) {
				//add the customer to the queue
				case "1":
					customerList = addCustomer(input);
					break;
				//remove the customer from the queue
				case "2":
					customerList = helpCustomer(customerList);
					break;
				//exit
				case"3":
					done = true;
					break;
				default:
					System.out.println("Sorry, there is not the option. Returning to menu.");					
			}
		}while(!done);			
		return toReport;
	}
	
	/*Method for store the serviced customers' list to a file */
	public static void writeReport(ArrayList<Customer> data, String filename) {
		try {
			FileWriter reports = new FileWriter (filename,true);
			for (int i=0; i<data.size();i++) {				
				reports.write((data.get(i)).toCSV()+"\n");//write the data of every customer to the file
			}
			reports.flush();
			reports.close();
		}catch(Exception e ) {
			System.out.println("Error in writing to a file.");
		}		
	}

	public static void main(String[] args) {
		System.out.println("The CustomerComeAgain Deli counter open for business.");
		
		//menu
		Scanner input = new Scanner(System.in);
		ArrayList<Customer> report = menu(input);
				
		System.out.println("The CustomerComeAgain Deli counter close business now. Welcome back tomorrow.");
		
		//send a report to a file
		System.out.print("Load the file to save the report: ");
		String file = input.nextLine();
     	writeReport(report,file);
		
		System.out.println("The programming is ending now.");
		
	}

}
