# Lab 6

## Total

18/20

## Break Down

Deli Queue Application

- Loops to display options          2/2
- Adds customer to queue            6/6
- Removes customer from queue       4/6
- Use ArrayLists to store customers 6/6

## Comments

- Remove errors when the queue is empty. -2
