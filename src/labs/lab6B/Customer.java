package labs.lab6B;

public class Customer {
	/* initialize the attribute*/
	private String name;
	private String telephoneNumber;
	private String address;	
	
	//default constructor
	public Customer() {
		name = "Customer X";
		telephoneNumber = null;
		address = null;
	}
	
	public Customer(String name) {
		this.name = name;
		telephoneNumber = null;
		address = null;
	}
	
	//non-default constructor
	public Customer(String name, String address, String telephone) {
		this.name = name;
		this.telephoneNumber = telephone;
		this.address = address;
	}
		
	//Accessor for name
	public String getName() {
		return name;
	}
    //Mutator for name
	public void setName(String name) {
		this.name = name;
	}	  
	
	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}
	
	public boolean validNumber(String s) {
		boolean isNumber = true;
		for(int i=0; i<s.length()&&isNumber; i++) {			
			if(!(Character.isDigit(s.charAt(i)))){
				isNumber = false;
			}
		}
		return isNumber;
	}
	
	public void setTelephoneNumber(String telephoneNumber) {
		if(telephoneNumber.length()==7&&validNumber(telephoneNumber)) {
			this.telephoneNumber = telephoneNumber;
		}			
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	//equals method
	public boolean equals(Object obj) {
		if(!(obj instanceof Customer)) {
			return false;
		}
		Customer c = (Customer) obj;
		if (this.name!=c.getName()) {
			return false;
		}else if (this.telephoneNumber!=c.telephoneNumber) {
			return false;
		}else if (this.address!=c.address) {
			return false;
		}
		return true;
	}	
	
	public String toString() {
		return name+" address: "+address+" telephone: "+telephoneNumber;
	}
	
	public String toDelivery() {
		return "Address: " +this.address +"  Telephone Number: "+ this.telephoneNumber;
	}
	
	public String toCSV() {
		return name+","+telephoneNumber+","+address;
	}


}
