package labs.lab6B;

import java.util.Scanner;
import java.io.FileWriter;
import java.util.ArrayList;

public class DeliApplication {	
	
	static ArrayList<Queue> customerList = new ArrayList<Queue>();
	static ArrayList<Queue> toReport = new ArrayList<Queue>(20);
	
	/*Method for adding the customer to the queue.*/
	public static ArrayList<Queue> addCustomer(Scanner input) {
		//loop to add the customer to the queue until entering "exit" to exit loop to return to menu
		boolean exitQueue = false;
		do {
			System.out.println("Acquire Queue Number here. Enter exit to leave queue.");
			System.out.print("Name:");
			String name = input.nextLine();
			if (name.equalsIgnoreCase("exit")){
				exitQueue = true;
				break;
			}
			
			boolean noYorN = true;
			String yn = null;
			do {
				System.out.print("Do you want it to be deliveried:");
				yn = input.nextLine().toLowerCase();
				if(yn.equals("y")||yn.equals("yes")||yn.equals("n")||yn.equals("no")) {	
					noYorN = false;
					break;
				}else {
					System.out.println("Your choice is not yes or no. I don't understand.");
				}
			}while(noYorN);
			
			Queue c=new Queue();
			switch(yn) {
				case "y":
				case "yes":
					System.out.print("delivery address:");
					String address = input.nextLine();					
					boolean validTelephoneNo = false;
					String telephoneNo = null;
					do {
						System.out.print("telephone number:");
						telephoneNo = input.nextLine();
						if(telephoneNo.length()==7 && telephoneNo.matches("\\d+")) {
							validTelephoneNo = true;
						}else {
							System.out.println("There is not valid telephone number.");
						}
					}while(!validTelephoneNo);
					int num1 = 1;
					if (customerList.size()>0) {
						num1=customerList.get(customerList.size()-1).getQueueNumber()+1;	//use the last customer's queue number plus 1 as the queue number
					}			
					c = new Queue(name, address,telephoneNo, true, num1);
					break;
				case "n":
				case "no":
					int num2=1;//set the first customer's queue number as 1
					if (customerList.size()>0) {
						num2=customerList.get(customerList.size()-1).getQueueNumber()+1;	//use the last customer's queue number plus 1 as the queue number
					}			
					c = new Queue(name, num2);	
			}
			customerList.add(c);	
			if (c.isTakeaway()) {
				System.out.println("Dear "+c.toString() + ". There are "+ customerList.indexOf(c)+" customers before you. \n"
						+ "Please wait. We will arrange deliverying as soon as we can.");//print out the queue number for the customer
			}else {
				System.out.println("Dear "+c.toString() + ". There are "+ customerList.indexOf(c)+" customers before you. \n"
						+ "Please wait. We will service you as soon as we can.");//print out the queue number for the customer
			}
			System.out.println("");
		}while(!exitQueue);		
		return customerList;
	}
	
	/*Method for remove the customer from the queue*/
	public static ArrayList<Queue> helpCustomer(ArrayList<Queue> list){
		if(list.size()>0) {
			if(list.get(0).isTakeaway()) {
				System.out.println(list.get(0).toString()+"\n"+list.get(0).toDelivery());
			}else {
				System.out.println(list.get(0).toString()+" , it is your turn.");
			}		
			toReport.add(list.get(0));//add the serviced customer to the report
			list.remove(0);//remove the serviced from the queue
			System.out.println("");
			return list;
		}else {
			System.out.println("There is not customer waiting now.");
			return list;
			
		}
		
	}
	
	/*Method for menu*/
	public static ArrayList<Queue> menu(Scanner input){
		
		String[] options = {"Add customer to queue", "Help customer","Exit"};
		//loop the menu until enter option 3 to exit
		boolean done = false;
		do{
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1)+"."+options[i]);
			}
			System.out.print("Choice:");
			String choice = input.nextLine();
			switch(choice) {
				//add the customer to the queue
				case "1":
					customerList = addCustomer(input);
					break;
				//remove the customer from the queue
				case "2":
					customerList = helpCustomer(customerList);
					break;
				//exit
				case"3":
					done = true;
					break;
				default:
					System.out.println("Sorry, there is not the option. Returning to menu.");					
			}
		}while(!done);			
		return toReport;
	}
	
	/*Method for store the serviced customers' list to a file */
	public static void writeReport(ArrayList<Queue> data, String filename) {
		try {
			FileWriter reports = new FileWriter (filename, true);
			for (int i=0; i<data.size();i++) {				
				reports.write((data.get(i)).toCSV()+"\n");//write the data of every customer to the file
			}
			reports.flush();
			reports.close();
		}catch(Exception e ) {
			System.out.println("Error in writing to a file.");
		}		
	}

	public static void main(String[] args) {
		System.out.println("The CustomerComeAgain Deli counter open for business.");
		
		//menu
		Scanner input = new Scanner(System.in);
		ArrayList<Queue> report = menu(input);
				
		System.out.println("The CustomerComeAgain Deli counter close business now. Welcome back tomorrow.");
		
		//send a report to a file
		System.out.print("Load the file to save the report: ");
		String filename = input.nextLine();
     	writeReport(report,filename);
		
		System.out.println("The programming is ending now.");
		
	}

}
