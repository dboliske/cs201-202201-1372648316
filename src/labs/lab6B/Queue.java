package labs.lab6B;

import java.util.Date;

public class Queue {
	
	private Customer customer;
	private int queueNumber;
	private boolean  takeaway;
	private Date date;

	public Queue() {
		customer = new Customer();
		queueNumber = 0;
		takeaway = false;
		date = new Date();
	}
	
	public Queue(String name, int queueNo) {
		this.customer = new Customer(name);
		this.queueNumber = queueNo;
		this.takeaway = false;
		this.date = new Date();
	}
	
	public Queue(String name, String address, String telephoneNo, boolean takeaway, int queueNo) {
		this.customer = new Customer(name, address, telephoneNo);
		this.queueNumber = queueNo;
		this.takeaway = takeaway;
		this.date = new Date();
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(int queueNumber) {
		if(queueNumber>=0) {
			this.queueNumber = queueNumber;
		}
	}

	public boolean isTakeaway() {
		return takeaway;
	}

	public void setTakeaway(boolean takeaway) {
		this.takeaway = takeaway;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String toCSV() {
		return this.queueNumber+","+customer.toCSV() +","+takeaway+","+date;
	}
	
	public String toDelivery() {
		return this.customer.toDelivery();
	}
	
	public String toString() {
		return customer.getName()+", queue number "+queueNumber;
	}
	
}
