package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	//method to find the value in the array using the binary search
	public static int binarySearch(String[] data, int start, int end, String value) {
		int pos = -1;
		//initialize the index that find the value 
		if (end-start<=1) {
		//when there are less than 3 elements in the array, compare the value to the element directly
			if (data[start].equalsIgnoreCase(value)) {
				pos = start;
			} else if (data[end].equalsIgnoreCase(value)) {
				pos = end;
			}
		} else if(end-start>1){
		//when there are more than 3 elements find whether the value in the array by using the recursion
			int mid = (start + end)/2;
			//divide the array in the middle
			if (data[mid].equalsIgnoreCase(value)) {
			// when the value equals to the middle element in the array, return the middle
				pos = mid;				
			} else if (data[mid].compareToIgnoreCase(value)<0) {
			// when the value is larger than the middle element in the array, using the binary search in the back half
				pos = binarySearch(data,mid+1,end,value);
			} else {
			// when the value is less than the middle element in the array, using the binary search in the front half
				pos = binarySearch(data,start,mid-1,value);
			}
		}
		return pos;
	}
	
	public static int search(String[] data, String value) {
		//use binary search to find the value in the array in its array's length
		return binarySearch(data,0,data.length,value);		
	}	
		
	public static void main(String[] args) {
		String[] arrays = {"C", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the String that is looked for: ");
		String value = input.nextLine();
		//using the search method
		int pos = search(arrays, value);
		if (pos==-1) {
		// when the search return -1, tell the user that you did not find the value
			System.out.println("There is not the String "+value+" in the array.");			
		} else {
		//when the search return the index, tell the user what is the index of the value in the array
			System.out.println("The String "+value+" : index "+pos+" in the array");
		}
		input.close();

	}

}
