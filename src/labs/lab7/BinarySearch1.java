package labs.lab7;

import java.util.Scanner;

public class BinarySearch1 {
//binary search using no recursion
	
	public static int binarySearch(String[] data, int i, int j, String value) {
	//check the String in the array between index i and j using the binary search
		int start = i;
		int end = j;				
		
		while(start<=end) {
		//loop while there are still elements in the array
			if(end-start<=1) {
			//when there less than 3 elements in the array, check whether the value is in the array directly
				if(data[start].equalsIgnoreCase(value)) {
				   return start;
				} else if (data[end].equalsIgnoreCase(value)) {
					return end;
				} else {
					return -1;
				}
			}
			//when there are 3 or more than 3 elements in the arrays, divide the array into 2 half
			int mid = (start+end)/2;	
			//if the value is before the middle element, check only the front half
			if(value.compareToIgnoreCase(data[mid])<0){
				end = mid-1;
			} else if (value.compareToIgnoreCase(data[mid])>0) {
			//if the value is after the middle, check only the end half
				start = mid+1;				
			} else {
			//if the value equals to the middle element, return middle
				return mid;				
			}			
		}
		// when not finding the value, return -1
		return -1;
		
	}

	public static int search(String[] data, String value) {
		int index = binarySearch(data, 0, data.length, value);		
		return index;
	}
	
	public static void main(String[] args) {
		String[] arrays = { "c", "html", "java", "python", "ruby", "scala" };
		System.out.print("Enter the String that need to be found: ");
		Scanner input = new Scanner (System.in);
		String value = input.nextLine();
		int pos = search(arrays, value);
		if (pos==-1) {
			System.out.println("The String "+value+" is not in the array.");
		} else {
			System.out.println("The String "+value+"'s index is: "+pos);
		}
		input.close();

	}

}
