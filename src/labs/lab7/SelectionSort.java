package labs.lab7;

public class SelectionSort {

	public static double[] sort(double[] data) {
	//sort an array using selection sort
		for(int i=0; i<data.length-1; i++) {
		//loop to find the minimum in the unsorted to put in the sorted section 
			int min = i;
			//suppose the element at i is the minimum
			for( int j=i+1; j<data.length; j++) {
			//loop from the i+1 to the end of the array to find the real minimum
				if (data[j]<data[min]) {
					min = j;
				}
			}
			if(min!=i) {
			//if the minimum is not the element at i, swap the elements between i and min
				double temp = data[i];
				data[i] = data[min];
				data[min] = temp;				
			}
		}
		return data;
	}
	
	public static void main(String[] args) {
		double[] arrays = {3.142,  2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		arrays = sort (arrays);
		for(int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+", ");
		}
	}

}
