package labs.lab7;

public class SelectionSort1 {
//selection sort using recursion
	
	public static int findMin(double[] data, int start, int end) {
	// find the minimum of the array between index start and end
		int min = start;
		for(int i=start+1; i<data.length; i++) {
			if(data[i]<data[min]) {
				min = i;
			}
		}
		return min;
	}
	
	public static void selectionSort(double[] data, int start, int end) {
	//selection sort using recursion
		int min = findMin(data, start, end);
		//find the array's minimum between the index start and end
		if(start!= min) {
		//when the minimum is not the element in the start, swap the minimum with the element at the start
			swap(data, min, start);
		}
		if(end-start>1) {
		//when the array is larger than 3 elements, using recursion
			selectionSort(data, start+1, end);
		} 
	}
	
	public static void swap(double[] data, int i, int j) {
	// swap the elements between the index i and j
		double temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}
	
	public static double[] sort(double[] data) {
	//sort an array using selection sort
		selectionSort(data, 0, data.length);
		return data;
	}

	public static void main(String[] args) {
		double[] arrays = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		arrays = sort(arrays);
		for (int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+ " ");
		}
		

	}

}
