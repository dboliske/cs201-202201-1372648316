package labs.lab7;

public class bubbleSort {

	public static int[] sort(int[] data) {
		//sort an array using bubble sort
		boolean swapped;
		do {
		//when swapped the adjacent elements loop until there is no swapping 
			swapped = false;
			for(int i=0; i<data.length-1;i++) {
			//check from the beginning to the end of the array to see whether the elements in the array is in the correct sequence	
				if(data[i]>data[i+1]) {
				//when the adjacent elements is incorrect, swap them
					int temp = data[i];
					data[i] = data[i+1];
					data[i+1] = temp;
					swapped = true;
				}				
			}			
		} while(swapped);	
		return data;
	}
	
	public static void main(String[] args) {
		int[] arrays = {10,4,7,3,8,6,1,2,5,9};
		arrays = sort(arrays);
		for(int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+" ");
		}
	}

}
