package labs.lab7;

public class bubbleSort1 {

	public static int[] sort(int[] data) {
	//bubble sort using the nested for loop
		for (int i=0; i<data.length-1;i++) {
			boolean swapped = false;
			for(int j=0; j<data.length-1-i;j++) {
			//check from the beginning to the array's length minus the times in iteration
				if (data[j]>data[j+1]) {
					int temp = data[j];
					data[j] = data[j+1];
					data[j+1] = temp;
					swapped =true;
				}
			}
			//when the loop from the beginning to the last unsorted element has not swapping, exiting the loop
			if(!swapped) {
				break;
			}			
		 }
		 return data;
	}
	
	public static void main(String[] args) {
		int[] arrays = {10, 4,7,3,8,6,1,2,5,9};
		arrays = sort (arrays);
		for(int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+" ");
		}
	}

}
