package labs.lab7;

public class insertionSort {

	public static String[] sort(String[] data) {
	//sort a String array using insertion sort
		for(int i=1; i<data.length; i++) {
		//select the first element in the unsorted section and insert into the correct spot in the sorted selection
			int j=i;
			while(j>=1 && data[j-1].compareToIgnoreCase(data[j])>0 ) {
			//if the element from the unsorted is less than the element in the sorted section, swap it until it is in the correct spot
				String temp = data[j];
				data[j] = data[j-1];
				data[j-1] = temp;	
				j--;
			}			
		}
		return data;		
	}
	
	public static void main(String[] args) {
		String[] arrays = {"cat","fat","dog","apple","bat","egg"};
		arrays = sort(arrays);
		for(int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+" ");
		}
		

	}

}
