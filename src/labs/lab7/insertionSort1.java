package labs.lab7;

public class insertionSort1 {

	public static String[] sort(String[] data) {
	//insertion sort using shifting down the elements bigger than the element from the unsorted
		for(int i=1; i<data.length; i++) {
		//select the first element in the unsorted section
			String temp = data[i];
			int j=i;
			while(j>=1 && data[j-1].compareToIgnoreCase(temp)>=0){
			/*loop from the end of the sorted section to the start 
			 * and shift back the elements in the sorted section that is bigger than the element from the unsorted section
			 */
				 data[j]=data[j-1];				
				 j=j-1;
				}	
			if(!temp.equals(data[i])) {
			// insert the element from the unsorted into the correct spot
				data[j]=temp;
			}			
		}
		return data;
	}
	
	public static void main(String[] args) {
		String[] arrays = {"cat", "fat","dog","apple","bat","egg"};
		arrays = sort(arrays);
		for(int i=0; i<arrays.length; i++) {
			System.out.print(arrays[i]+" ");
		}
	}

}
